use crate::hash::*;
use crate::render::TextureId;

#[derive(Copy, Clone)]
pub struct Sprite {
    pub scale: glm::Vec2,
    pub offset: glm::Vec2,
    pub texture: Option<TextureId>,
    pub horizontal_flip: bool,
    pub vertical_flip: bool,
}

impl TypeHash for Sprite {
    fn type_hash() -> crate::hash::Hash {
        hash!("Sprite")
    }
}

#[derive(Copy, Clone)]
pub struct SubRectSprite {
    pub start: glm::IVec2,
    pub size: glm::IVec2,
}

impl TypeHash for SubRectSprite {
    fn type_hash() -> crate::hash::Hash {
        hash!("SubRectSprite")
    }
}
#[derive(Copy, Clone)]
pub struct AnimatedSprite {
    pub animation: super::db::SpriteAnimationId,
    pub animation_clock: i32,
}

impl TypeHash for AnimatedSprite {
    fn type_hash() -> crate::hash::Hash {
        hash!("AnimatedSprite")
    }
}
#[derive(Copy, Clone)]

pub enum AnimationMode {
    Loop,
    Stop,
    Bounce,
}
#[derive(Copy, Clone)]
pub struct SpriteAnimation {
    pub start: glm::IVec2,
    pub size: glm::IVec2,
    pub frame_count: i32,
    pub tick_rate: i32,
    pub mode: AnimationMode,
    pub reversed: bool,
    pub texture: TextureId, //why is this not using the one in Sprite?
    pub horizontal_flip: bool,
}

impl SpriteAnimation {
    pub fn time_to_frame(&self, time: i32) -> u32 {
        let bounce_func = |i, d, mut f| {
            //thanks jefvel for everything...
            if f == 1 {
                //f == 1 here to ensure that f is not 0 after this which means that we get a div/0 below.
                return 0;
            }

            f = f - 1;
            let step = i / d;
            let mut mod_step: i32 = step % (f * 2);
            mod_step -= f;
            if mod_step < 0 {
                mod_step = -mod_step;
            }
            mod_step = f - mod_step;

            mod_step
        };

        let maybe_reversed = |val| match self.reversed {
            true => (self.frame_count - 1) - val,
            false => val,
        };

        maybe_reversed(match self.mode {
            AnimationMode::Loop => (time / self.tick_rate) % self.frame_count,
            AnimationMode::Stop => (time / self.tick_rate).min(self.frame_count - 1),
            AnimationMode::Bounce => bounce_func(time, self.tick_rate, self.frame_count),
        }) as u32
    }
}
