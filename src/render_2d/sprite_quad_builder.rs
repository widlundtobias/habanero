use super::*;
use crate::render::*;
use crate::spatial_2d::*;
use dpx_db::table::Table;
use dpx_macros::join;

pub struct SpriteQuadBuilder {}

impl SpriteQuadBuilder {
    pub fn new() -> Self {
        Self {}
    }
    fn generate_quads<'a>(
        &self,
        target: &mut Vec<Quad<'a>>,
        camera: &Camera,
        tables: (
            &TWorldPosition2d,
            &TWorldRotation2d,
            &TWorldDepth,
            &TSprite,
            &TDrawableShader,
            &TDrawableColor,
            &TSubRectSprite,
            &TAnimatedSprite,
            &'a TShader,
            &'a TTexture,
            &TSpriteAnimation,
        ),
    ) {
        let (
            pos_t,
            rot_t,
            depth_t,
            sprite_t,
            drawable_shader_t,
            drawable_color_t,
            sub_rect_sprite_t,
            animated_sprite_t,
            shader_t,
            texture_t,
            sprite_animation_t,
        ) = tables;

        let camera_facing = false; //should somehow be a setting

        target.reserve(sprite_t.len());

        let mut occurred_z_min = f32::MAX;
        let mut occurred_z_max = f32::MIN;

        join! {joined_iter = JoinEntry, sprite_t: sprite, drawable_shader_t: drawable_shader, pos_t: pos, rot_t: rot, depth_t: depth};

        for JoinEntry {
            id,
            sprite,
            pos,
            rot,
            depth,
            drawable_shader,
        } in joined_iter
        {
            target.push(Quad::new_with_shader(
                shader_t.get(drawable_shader.shader).unwrap(),
            ));
            let current_quad = target.last_mut().unwrap();

            let mut texture_id = sprite.texture;

            //we need to find if it's an animated sprite because their texture overrides the normal texture. but WHY????
            let animated_sprite = animated_sprite_t.get(id);
            if let Some(animated_sprite) = animated_sprite {
                texture_id = Some(
                    sprite_animation_t
                        .get(animated_sprite.animation)
                        .unwrap()
                        .texture,
                );
            }

            current_quad.texture = texture_id.map(|id| texture_t.get(id).unwrap());

            //Z determines the paint order of the quads. if we render in camera-facing mode, we get Z from the distance to the camera
            //otherwise the application decides the Z order using the sprite depth
            current_quad.z = match camera_facing {
                true => -glm::distance(
                    &camera.translation,
                    &glm::vec3(pos.coord.x, pos.coord.y, depth.value),
                ),
                false => depth.value,
            };
            occurred_z_max = occurred_z_max.max(current_quad.z);
            occurred_z_min = occurred_z_min.min(current_quad.z);

            current_quad.position = pos.coord + sprite.offset;
            current_quad.rotation = rot.angle;

            current_quad.color = match drawable_color_t.get(id) {
                Some(c) => c.color,
                None => crate::color::white(),
            };

            let texture_size: glm::Vec2 = texture_id
                .map(|id| glm::convert(texture_t.get(id).unwrap().size))
                .unwrap_or(glm::vec2(1.0, 1.0));

            let sub_rect_sprite = sub_rect_sprite_t.get(id);

            let mut horizontal_flip = sprite.horizontal_flip;

            match (sub_rect_sprite, animated_sprite) {
                (None, None) => {
                    current_quad.texture_start = glm::vec2(0.0, 0.0);
                    current_quad.texture_end = glm::vec2(1.0, 1.0);
                    current_quad.size = match sprite.texture {
                        Some(_) => texture_size.component_mul(&sprite.scale),
                        None => sprite.scale,
                    };
                }
                (Some(sub_rect_sprite), None) => {
                    let frame_start: glm::Vec2 = glm::convert(sub_rect_sprite.start);
                    let frame_end: glm::Vec2 =
                        glm::convert(sub_rect_sprite.start + sub_rect_sprite.size);

                    current_quad.texture_start = frame_start.component_div(&texture_size);
                    current_quad.texture_end = frame_end.component_div(&texture_size);
                    current_quad.size = sprite
                        .scale
                        .component_mul(&glm::convert::<_, glm::Vec2>(sub_rect_sprite.size));
                }
                (None, Some(animated_sprite)) => {
                    let animation = sprite_animation_t.get(animated_sprite.animation).unwrap();

                    let current_frame = animation.time_to_frame(animated_sprite.animation_clock);

                    let frame_start =
                        glm::vec2(animation.start.x * current_frame as i32, animation.start.y);
                    let frame_end = frame_start + animation.size;

                    current_quad.texture_start =
                        texture_size.component_div(&glm::convert::<_, glm::Vec2>(frame_start));
                    current_quad.texture_end =
                        texture_size.component_div(&glm::convert::<_, glm::Vec2>(frame_end));
                    current_quad.size = sprite
                        .scale
                        .component_mul(&glm::convert::<_, glm::Vec2>(animation.size));

                    if animation.horizontal_flip {
                        horizontal_flip = !horizontal_flip;
                    }
                }
                _ => panic!("Invalid sprite configuration"),
            }

            if horizontal_flip {
                std::mem::swap(
                    &mut current_quad.texture_start.x,
                    &mut current_quad.texture_end.x,
                )
            }
            if sprite.vertical_flip {
                std::mem::swap(
                    &mut current_quad.texture_start.y,
                    &mut current_quad.texture_end.y,
                );
            }
        }
    }
}

pub const SPRITE_QUAD_BUILDER_ID: u32 = 0;
impl QuadBuilder for SpriteQuadBuilder {
    fn unique_id(&self) -> u32 {
        SPRITE_QUAD_BUILDER_ID
    }
}

pub struct SpriteQuadBuilderDispatcher<'a> {
    pub tables: (
        &'a TWorldPosition2d,
        &'a TWorldRotation2d,
        &'a TWorldDepth,
        &'a TSprite,
        &'a TDrawableShader,
        &'a TDrawableColor,
        &'a TSubRectSprite,
        &'a TAnimatedSprite,
        &'a TShader,
        &'a TTexture,
        &'a TSpriteAnimation,
    ),
}

impl<'a> QuadBuilderDispatcher<'a> for SpriteQuadBuilderDispatcher<'a> {
    fn dispatch_generate_quads(
        &self,
        target: &mut Vec<Quad<'a>>,
        camera: &Camera,
        builder: &dyn QuadBuilder,
    ) {
        builder
            .as_any()
            .downcast_ref::<SpriteQuadBuilder>()
            .unwrap()
            .generate_quads(target, camera, self.tables);
    }
}
