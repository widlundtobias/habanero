use super::*;
use crate::entity::EntityId;
use crate::entity::EntityProperties;
use crate::hash::TypeHash;
use crate::render::*;
use crate::spatial_2d::new_spatial_properties;

pub fn new_sprite_properties(
    position: glm::Vec2,
    depth: f32,
    rotation: Option<f32>,
    parent: Option<EntityId>,
    scale: glm::Vec2,
    texture: Option<TextureId>,
    shader: ShaderId,
) -> EntityProperties {
    let mut props = new_spatial_properties(position, depth, rotation, parent);

    crate::add_property!(props, DrawableShader { shader });
    crate::add_property!(
        props,
        Sprite {
            scale,
            offset: glm::zero(),
            texture,
            horizontal_flip: false,
            vertical_flip: false,
        }
    );

    props
}

pub fn new_sub_rect_sprite_properties(
    position: glm::Vec2,
    depth: f32,
    rotation: Option<f32>,
    parent: Option<EntityId>,
    scale: glm::Vec2,
    texture: Option<TextureId>,
    shader: ShaderId,
    sub_rect_start: glm::IVec2,
    sub_rect_size: glm::IVec2,
) -> EntityProperties {
    let mut props =
        new_sprite_properties(position, depth, rotation, parent, scale, texture, shader);

    crate::add_property!(
        props,
        SubRectSprite {
            start: sub_rect_start,
            size: sub_rect_size,
        }
    );

    props
}

pub fn new_animated_sprite_properties(
    position: glm::Vec2,
    depth: f32,
    rotation: Option<f32>,
    parent: Option<EntityId>,
    scale: glm::Vec2,
    texture: Option<TextureId>,
    shader: ShaderId,
    animation: SpriteAnimationId,
) -> EntityProperties {
    let mut props =
        new_sprite_properties(position, depth, rotation, parent, scale, texture, shader);

    crate::add_property!(
        props,
        AnimatedSprite {
            animation,
            animation_clock: 0
        }
    );

    props
}
//pub fn new_set_animated_sprite_properties() -> EntityProperties {}
pub fn new_text_properties(
    position: glm::Vec2,
    depth: f32,
    rotation: Option<f32>,
    parent: Option<EntityId>,
    text: &str,
    size: u32,
    font: FontId,
    shader: ShaderId,
) -> EntityProperties {
    let mut props = new_spatial_properties(position, depth, rotation, parent);

    crate::add_property!(props, DrawableShader { shader });

    crate::add_property!(
        props,
        Text {
            text: text.to_string(),
            font,
            font_size: size
        }
    );

    props
}
