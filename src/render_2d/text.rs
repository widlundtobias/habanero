use crate::hash::TypeHash;
use crate::render::FontId;
pub struct Text {
    pub text: String,
    pub font: FontId,
    pub font_size: u32,
}

impl TypeHash for Text {
    fn type_hash() -> crate::hash::Hash {
        hash!("Text")
    }
}
