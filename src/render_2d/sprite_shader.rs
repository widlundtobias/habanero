pub const VERTEX_SOURCE: &str = "#version 300 es

layout(location=0) in vec4 position; //will be compatible with vec2 and vec3 attributes. extended to vec4: (0, 0, 0, 1) for missing components
layout(location=1) in vec2 texCoord;
layout(location=2) in vec4 color;

uniform mat4 viewProjection;

out vec2 vTex;
out vec4 vColor;

void main()
{
    gl_Position = viewProjection * position;
    vTex = texCoord;
    vColor = color;
}
";

pub const FRAGMENT_SOURCE: &str = "#version 300 es

precision mediump float;

uniform sampler2D uTexture;

in vec2 vTex;
in vec4 vColor;

out vec4 outColor;

void main()
{
    outColor = texture(uTexture, vTex) * vColor;
}
";

use crate::gl::ShaderAttributeBinding;

pub fn attribute_bindings() -> Vec<ShaderAttributeBinding> {
    vec![
        ShaderAttributeBinding {
            name: String::from("position"),
            index: 0,
        },
        ShaderAttributeBinding {
            name: String::from("texCoord"),
            index: 1,
        },
        ShaderAttributeBinding {
            name: String::from("color"),
            index: 2,
        },
    ]
}

pub fn new_sprite_shader(gl: std::rc::Rc<crate::gl::Gl>) -> crate::gl::Shader {
    crate::gl::Shader::new(
        VERTEX_SOURCE,
        FRAGMENT_SOURCE,
        attribute_bindings().as_slice(),
        gl,
    )
    .unwrap() //unwrap since the inputs are hard coded
}
