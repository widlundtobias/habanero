use super::*;
use crate::entity::EntityId;
use crate::table::{TableVisitablePerId, TableVisitorPerId};
use dpx_db::mapvectable::MapVecTable;
use dpx_db::table;
use std::num::NonZeroUsize;

pub type TSprite = MapVecTable<EntityId, Sprite, table::ManualIdTag>;
pub type TSubRectSprite = MapVecTable<EntityId, SubRectSprite, table::ManualIdTag>;
pub type TAnimatedSprite = MapVecTable<EntityId, AnimatedSprite, table::ManualIdTag>;

pub type SpriteAnimationId = NonZeroUsize;
pub type TSpriteAnimation = MapVecTable<SpriteAnimationId, SpriteAnimation, table::AutoIdTag>;

pub struct CoreModule {
    pub sprite_t: TSprite,
    pub sub_rect_sprite_t: TSubRectSprite,
    pub animated_sprite_t: TAnimatedSprite,
    pub sprite_animation_t: TSpriteAnimation,
}

impl CoreModule {
    pub fn new() -> Self {
        Self {
            sprite_t: TSprite::new(),
            sub_rect_sprite_t: TSubRectSprite::new(),
            animated_sprite_t: TAnimatedSprite::new(),
            sprite_animation_t: TSpriteAnimation::new(),
        }
    }
}

impl TableVisitablePerId<EntityId> for CoreModule {
    fn visit_manual_id_mut<Visitor: TableVisitorPerId<EntityId>>(&mut self, visitor: &Visitor) {
        visitor.visit_manual_id_mut(&mut self.sprite_t);
        visitor.visit_manual_id_mut(&mut self.sub_rect_sprite_t);
        visitor.visit_manual_id_mut(&mut self.animated_sprite_t);
    }
    fn visit_manual_id<Visitor: TableVisitorPerId<EntityId>>(&self, visitor: &Visitor) {
        visitor.visit_manual_id(&self.sprite_t);
        visitor.visit_manual_id(&self.sub_rect_sprite_t);
        visitor.visit_manual_id(&self.animated_sprite_t);
    }
}
