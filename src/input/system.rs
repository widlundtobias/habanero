use super::*;
pub struct InputSystem {
    state: InternalRawInputState,
}

//TODO: mouse capture
//TODO: system input?
//TODO: imgui??? although not yet

impl InputSystem {
    pub fn new(sdl: &sdl2::Sdl) -> Self {
        let mouse_x = sdl.event_pump().unwrap().mouse_state().x();
        let mouse_y = sdl.event_pump().unwrap().mouse_state().y();

        let state = InternalRawInputState::new(glm::vec2(mouse_x, mouse_y));

        Self { state }
    }

    pub fn update_input_state(&mut self, sdl: &sdl2::Sdl) -> RawInput {
        let mut res = RawInput::new(self.state.cursor_position);

        for event in sdl.event_pump().unwrap().poll_iter() {
            match event {
                sdl2::event::Event::Quit { .. } => res.system_quit = Some(()),
                sdl2::event::Event::Window {
                    win_event: sdl2::event::WindowEvent::SizeChanged(x, y),
                    ..
                } => res.window_resized = Some(glm::vec2(x, y)),
                sdl2::event::Event::KeyDown {
                    scancode: Some(s),
                    repeat: false, //make sure it's the first press of this key, not repeats from holding
                    ..
                } => {
                    res.started_key_presses.insert(s);
                    self.state.active_key_presses.insert(s);
                }
                sdl2::event::Event::KeyUp {
                    scancode: Some(s), ..
                } => {
                    res.started_key_presses.remove(&s);
                    self.state.active_key_presses.remove(&s);
                    res.stopped_key_presses.insert(s);
                }
                sdl2::event::Event::MouseButtonDown { mouse_btn: b, .. } => {
                    res.started_mouse_presses.insert(b);
                    self.state.active_mouse_presses.insert(b);
                }
                sdl2::event::Event::MouseButtonUp { mouse_btn: b, .. } => {
                    res.started_mouse_presses.remove(&b);
                    self.state.active_mouse_presses.remove(&b);
                    res.stopped_mouse_presses.insert(b);
                }
                sdl2::event::Event::MouseMotion {
                    xrel, yrel, x, y, ..
                } => {
                    self.state.cursor_position = glm::vec2(x, y);
                    res.cursor_delta = Some(glm::vec2(xrel, yrel));
                }
                sdl2::event::Event::MouseWheel { y, .. } => res.wheel_delta = Some(y),
                _ => {}
            }
        }

        res.active_key_presses = self.state.active_key_presses.clone();
        res.active_mouse_presses = self.state.active_mouse_presses.clone();
        res.cursor_position = self.state.cursor_position.clone();

        /*TBI bool controlPressed = mData.ongoingKeyPresses.count(KeyCode::LeftControl) == 1 || mData.ongoingKeyPresses.count(KeyCode::RightControl) == 1;
        bool shiftPressed = mData.ongoingKeyPresses.count(KeyCode::LeftShift) == 1 || mData.ongoingKeyPresses.count(KeyCode::RightShift) == 1;
        bool altPressed = mData.ongoingKeyPresses.count(KeyCode::LeftAlt) == 1 || mData.ongoingKeyPresses.count(KeyCode::RightAlt) == 1;*/

        res
    }
}
