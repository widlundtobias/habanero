use sdl2::keyboard::Scancode;
use sdl2::mouse::MouseButton;
use std::collections::HashSet;

pub struct InternalRawInputState {
    pub cursor_position: glm::IVec2,
    pub active_mouse_presses: HashSet<MouseButton>,
    pub active_key_presses: HashSet<Scancode>,
}

impl InternalRawInputState {
    pub fn new(cursor_position: glm::IVec2) -> Self {
        Self {
            cursor_position,
            active_mouse_presses: HashSet::new(),
            active_key_presses: HashSet::new(),
        }
    }
}

pub struct RawInput {
    ///in pixels, relative to top-left corner of the window which is 0,0
    pub cursor_position: glm::IVec2,
    ///how much the mouse position changed this frame
    pub cursor_delta: Option<glm::IVec2>,
    ///how much the mouse wheel changed this frame
    pub wheel_delta: Option<i32>,
    ///mouse button presses that are new to this frame
    pub started_mouse_presses: HashSet<MouseButton>,
    ///mouse buttons that are currently held down. Includes the ones started this frame
    pub active_mouse_presses: HashSet<MouseButton>,
    ///mouse buttons that are no longer held down, since this frame.
    pub stopped_mouse_presses: HashSet<MouseButton>,
    ///key presses that are new to this frame
    pub started_key_presses: HashSet<Scancode>,
    ///keys that are currently held down. Includes the ones started this frame
    pub active_key_presses: HashSet<Scancode>,
    ///keys that are no longer held down, since this frame
    pub stopped_key_presses: HashSet<Scancode>,
    ///quit from OS-side action requested (X in corner, alt-F4, etc)
    pub system_quit: Option<()>,
    ///window is resized
    pub window_resized: Option<glm::IVec2>,
}

impl RawInput {
    pub fn new(cursor_position: glm::IVec2) -> Self {
        Self {
            cursor_position,
            cursor_delta: None,
            wheel_delta: None,
            started_mouse_presses: HashSet::new(),
            active_mouse_presses: HashSet::new(),
            stopped_mouse_presses: HashSet::new(),
            started_key_presses: HashSet::new(),
            active_key_presses: HashSet::new(),
            stopped_key_presses: HashSet::new(),
            system_quit: None,
            window_resized: None,
        }
    }
}
