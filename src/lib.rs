extern crate nalgebra_glm as glm;

//always present modules
#[macro_use]
pub mod hash;
pub mod table;

#[cfg(feature = "entity")]
#[macro_use]
pub mod entity;
#[cfg(feature = "render")]
pub mod render;
#[cfg(feature = "render_2d")]
pub mod render_2d;
#[cfg(feature = "spatial_2d")]
pub mod spatial_2d;

//uncategorised:

pub mod app;
pub mod color;
pub mod gl;
pub mod input;
pub mod window;
