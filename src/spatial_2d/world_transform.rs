pub fn update_world_spatial(tables: &mut super::CoreModule) {
    //right now this is broken because it doesn't take parents into account
    //feel free to implement
    tables.world_depth_t.ids = tables.object_depth_t.ids.clone();
    tables.world_depth_t.index_map = tables.object_depth_t.index_map.clone();
    tables.world_depth_t.data = tables
        .object_depth_t
        .data
        .iter()
        .map(|e| super::WorldDepth { value: e.value })
        .collect();
    tables.world_position_2d_t.ids = tables.object_position_2d_t.ids.clone();
    tables.world_position_2d_t.index_map = tables.object_position_2d_t.index_map.clone();
    tables.world_position_2d_t.data = tables
        .object_position_2d_t
        .data
        .iter()
        .map(|e| super::WorldPosition2d { coord: e.coord })
        .collect();
    tables.world_rotation_2d_t.ids = tables.object_rotation_2d_t.ids.clone();
    tables.world_rotation_2d_t.index_map = tables.object_rotation_2d_t.index_map.clone();
    tables.world_rotation_2d_t.data = tables
        .object_rotation_2d_t
        .data
        .iter()
        .map(|e| super::WorldRotation2d { angle: e.angle })
        .collect();
}
