use crate::entity::EntityId;
use crate::hash::*;
use crate::table::{TableVisitablePerId, TableVisitorPerId};
use dpx_db::mapvectable::MapVecTable;
use dpx_db::table;

/// a parent-child relationship of this entity. describes which entity is the parent,
/// making this entity implicitly a child
#[derive(Debug, Clone, Copy)]
pub struct EntityParent {
    pub parent: EntityId,
}

impl TypeHash for EntityParent {
    fn type_hash() -> crate::hash::Hash {
        hash!("EntityParent")
    }
}
pub type TEntityParent = MapVecTable<crate::entity::EntityId, EntityParent, table::ManualIdTag>;

/// a relative position of an entity, to its parent. equal to absolute if no parent
#[derive(Debug, Clone, Copy)]
pub struct ObjectPosition2d {
    pub coord: glm::Vec2,
}

impl TypeHash for ObjectPosition2d {
    fn type_hash() -> crate::hash::Hash {
        hash!("ObjectPosition2d")
    }
}
pub type TObjectPosition2d =
    MapVecTable<crate::entity::EntityId, ObjectPosition2d, table::ManualIdTag>;
/// a relative rotation of an entity, to its parent. equal to absolute if no parent
#[derive(Debug, Clone, Copy)]
pub struct ObjectRotation2d {
    pub angle: f32,
}

impl TypeHash for ObjectRotation2d {
    fn type_hash() -> crate::hash::Hash {
        hash!("ObjectRotation2d")
    }
}
pub type TObjectRotation2d =
    MapVecTable<crate::entity::EntityId, ObjectRotation2d, table::ManualIdTag>;
/// a relative depth of an entity, to its parent. equal to absolute if no parent
#[derive(Debug, Clone, Copy)]
pub struct ObjectDepth {
    pub value: f32,
}

impl TypeHash for ObjectDepth {
    fn type_hash() -> crate::hash::Hash {
        hash!("ObjectDepth")
    }
}
pub type TObjectDepth = MapVecTable<crate::entity::EntityId, ObjectDepth, table::ManualIdTag>;
/// an absolute position of an entity
#[derive(Debug, Clone, Copy)]
pub struct WorldPosition2d {
    pub coord: glm::Vec2,
}

impl TypeHash for WorldPosition2d {
    fn type_hash() -> crate::hash::Hash {
        hash!("WorldPosition2d")
    }
}
pub type TWorldPosition2d =
    MapVecTable<crate::entity::EntityId, WorldPosition2d, table::ManualIdTag>;
/// an absolute rotation of an entity
#[derive(Debug, Clone, Copy)]
pub struct WorldRotation2d {
    pub angle: f32,
}

impl TypeHash for WorldRotation2d {
    fn type_hash() -> crate::hash::Hash {
        hash!("WorldRotation2d")
    }
}
pub type TWorldRotation2d =
    MapVecTable<crate::entity::EntityId, WorldRotation2d, table::ManualIdTag>;

/// an absolute depth of an entity
#[derive(Debug, Clone, Copy)]
pub struct WorldDepth {
    pub value: f32,
}

impl TypeHash for WorldDepth {
    fn type_hash() -> crate::hash::Hash {
        hash!("WorldDepth")
    }
}
pub type TWorldDepth = MapVecTable<crate::entity::EntityId, WorldDepth, table::ManualIdTag>;

//#[derive(DpxDbModule)]
//#[db_tables(TRenderer, TRenderPass)]
/// contains the rendering tables common to all rendering
pub struct CoreModule {
    pub entity_parent_t: TEntityParent,
    pub object_position_2d_t: TObjectPosition2d,
    pub object_rotation_2d_t: TObjectRotation2d,
    pub object_depth_t: TObjectDepth,
    pub world_position_2d_t: TWorldPosition2d,
    pub world_rotation_2d_t: TWorldRotation2d,
    pub world_depth_t: TWorldDepth,
}

impl CoreModule {
    pub fn new() -> Self {
        Self {
            entity_parent_t: TEntityParent::new(),
            object_position_2d_t: TObjectPosition2d::new(),
            object_rotation_2d_t: TObjectRotation2d::new(),
            object_depth_t: TObjectDepth::new(),
            world_position_2d_t: TWorldPosition2d::new(),
            world_rotation_2d_t: TWorldRotation2d::new(),
            world_depth_t: TWorldDepth::new(),
        }
    }
}

impl TableVisitablePerId<EntityId> for CoreModule {
    fn visit_manual_id_mut<Visitor: TableVisitorPerId<EntityId>>(&mut self, visitor: &Visitor) {
        visitor.visit_manual_id_mut(&mut self.entity_parent_t);
        visitor.visit_manual_id_mut(&mut self.object_position_2d_t);
        visitor.visit_manual_id_mut(&mut self.object_rotation_2d_t);
        visitor.visit_manual_id_mut(&mut self.object_depth_t);
        visitor.visit_manual_id_mut(&mut self.world_position_2d_t);
        visitor.visit_manual_id_mut(&mut self.world_rotation_2d_t);
        visitor.visit_manual_id_mut(&mut self.world_depth_t);
    }
    fn visit_manual_id<Visitor: TableVisitorPerId<EntityId>>(&self, visitor: &Visitor) {
        visitor.visit_manual_id(&self.entity_parent_t);
        visitor.visit_manual_id(&self.object_position_2d_t);
        visitor.visit_manual_id(&self.object_rotation_2d_t);
        visitor.visit_manual_id(&self.object_depth_t);
        visitor.visit_manual_id(&self.world_position_2d_t);
        visitor.visit_manual_id(&self.world_rotation_2d_t);
        visitor.visit_manual_id(&self.world_depth_t);
    }
}
