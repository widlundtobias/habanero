use crate::entity::EntityId;
use crate::entity::EntityProperties;
use crate::hash::TypeHash;
use crate::spatial_2d::EntityParent;
use crate::spatial_2d::ObjectDepth;
use crate::spatial_2d::ObjectPosition2d;
use crate::spatial_2d::ObjectRotation2d;

pub fn new_spatial_properties(
    position: glm::Vec2,
    depth: f32,
    rotation: Option<f32>,
    parent: Option<EntityId>,
) -> EntityProperties {
    let mut props = EntityProperties::new();

    crate::add_property!(props, ObjectPosition2d { coord: position });
    crate::add_property!(
        props,
        ObjectRotation2d {
            angle: rotation.unwrap_or(0.0)
        }
    );
    crate::add_property!(props, ObjectDepth { value: depth });

    if let Some(parent) = parent {
        crate::add_property!(props, EntityParent { parent });
    }

    props
}
