use std::num::NonZeroUsize;

pub type EntityId = NonZeroUsize;

pub use self::entity_properties::*;

#[macro_use]
mod entity_properties;
