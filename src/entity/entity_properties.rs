use super::*;
use crate::hash::TypeHash;
use crate::table::{TableVisitablePerId, TableVisitorPerId};

pub type EntityProperties = std::collections::HashMap<crate::hash::Hash, Box<dyn std::any::Any>>;

#[macro_export]
macro_rules! add_property {(
    $target:ident,
    $($ty:ident)::* {$($rest: tt)*}
) => (
    $target.insert(
        $($ty)::* ::type_hash(),
        Box::new($($ty)::* { $($rest)* }),
    );
)}

pub fn insert_from_props(
    id: EntityId,
    module: &mut impl TableVisitablePerId<EntityId>,
    props: &EntityProperties,
) {
    struct PropsInserter<'a> {
        id: EntityId,
        props: &'a EntityProperties,
    }

    impl<'a> PropsInserter<'a> {
        fn new(id: EntityId, props: &'a EntityProperties) -> Self {
            Self { id, props }
        }
    }

    impl<'a> TableVisitorPerId<EntityId> for PropsInserter<'a> {
        fn visit_manual_id_mut<T: std::clone::Clone + TypeHash + 'static>(
            &self,
            table: &mut impl dpx_db::table::ManualId<Id = EntityId, TableItem = T>,
        ) {
            let hash = T::type_hash();
            if let Some(data) = self.props.get(&hash) {
                let data = data.downcast_ref::<T>().unwrap().clone();
                table.insert(self.id, data);
            }
        }
    }

    let visitor = PropsInserter::new(id, &props);

    module.visit_manual_id_mut(&visitor);
}
