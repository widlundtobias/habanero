#[derive(Copy, Clone, Debug, Eq, PartialEq, Default)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Color {
    pub fn with_alpha(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self { r, g, b, a }
    }
    pub fn opaque(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b, a: 255 }
    }

    pub fn from_hex(hex: u32) -> Self {
        Self {
            r: ((hex >> 24) & 0xFF) as u8,
            g: ((hex >> 16) & 0xFF) as u8,
            b: ((hex >> 8) & 0xFF) as u8,
            a: (hex & 0xFF) as u8,
        }
    }
    pub fn to_vec(&self) -> glm::U8Vec4 {
        glm::vec4(self.r, self.g, self.b, self.a)
    }

    pub fn as_ptr(&self) -> *const u8 {
        &self.r as *const u8
    }
}

impl From<NColor> for Color {
    fn from(c: NColor) -> Self {
        Self {
            r: denormalize_component(c.r),
            g: denormalize_component(c.g),
            b: denormalize_component(c.b),
            a: denormalize_component(c.a),
        }
    }
}

/// Normalized color values in an f32 between 0.0-1.0
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct NColor {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

impl NColor {
    pub fn with_alpha(r: f32, g: f32, b: f32, a: f32) -> Self {
        Self { r, g, b, a }
    }
    pub fn opaque(r: f32, g: f32, b: f32) -> Self {
        Self { r, g, b, a: 1.0 }
    }
    pub fn to_vec(&self) -> glm::Vec4 {
        glm::vec4(self.r, self.g, self.b, self.a)
    }

    pub fn as_ptr(&self) -> *const f32 {
        &self.r as *const f32
    }
}

//this is OK becuase even though floats are not Eq due to NaN and inf,
// the NColor type is assumed to never have those values
impl Eq for NColor {}

impl From<Color> for NColor {
    fn from(c: Color) -> Self {
        Self {
            r: normalize_component(c.r),
            g: normalize_component(c.g),
            b: normalize_component(c.b),
            a: normalize_component(c.a),
        }
    }
}

pub fn normalize_component(c: u8) -> f32 {
    c as f32 / 255.0
}
pub fn denormalize_component(c: f32) -> u8 {
    let c = c.max(0.0).min(1.0); //is there a faster way to do saturating casts?
    (c * 255.0) as u8
}

/// this is UB if c is not in the inclusive range of 0.0..1.0
pub unsafe fn denormalize_component_unchecked(c: f32) -> u8 {
    (c * 255.0) as u8
}

pub fn black() -> Color {
    Color::opaque(0, 0, 0)
}
pub fn gray() -> Color {
    Color::opaque(128, 128, 128)
}
pub fn white() -> Color {
    Color::opaque(255, 255, 255)
}
pub fn transparent() -> Color {
    Color::with_alpha(0, 0, 0, 0)
}
pub fn red() -> Color {
    Color::opaque(255, 0, 0)
}
pub fn green() -> Color {
    Color::opaque(0, 255, 0)
}
pub fn blue() -> Color {
    Color::opaque(0, 0, 255)
}
pub fn yellow() -> Color {
    Color::opaque(255, 255, 0)
}
pub fn magenta() -> Color {
    Color::opaque(255, 0, 255)
}
pub fn cyan() -> Color {
    Color::opaque(0, 255, 255)
}
pub fn pink() -> Color {
    Color::opaque(255, 192, 203)
}
pub fn brown() -> Color {
    Color::opaque(157, 97, 42)
}
pub fn tan() -> Color {
    Color::opaque(210, 180, 140)
}
pub fn orange() -> Color {
    Color::opaque(255, 165, 0)
}
pub fn teal() -> Color {
    Color::opaque(0, 128, 128)
}
pub fn purple() -> Color {
    Color::opaque(128, 0, 128)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_hex() {
        assert_eq!(Color::from_hex(0x00000000), Color::with_alpha(0, 0, 0, 0));
        assert_eq!(Color::from_hex(0xFF000000), Color::with_alpha(255, 0, 0, 0));
        assert_eq!(Color::from_hex(0x00FF0000), Color::with_alpha(0, 255, 0, 0));
        assert_eq!(Color::from_hex(0x0000FF00), Color::with_alpha(0, 0, 255, 0));
        assert_eq!(Color::from_hex(0x000000FF), Color::with_alpha(0, 0, 0, 255));
        assert_eq!(Color::from_hex(0x01020304), Color::with_alpha(1, 2, 3, 4));
    }
}
