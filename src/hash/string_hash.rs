#[derive(Copy, Clone, Hash, Ord, PartialOrd, Eq, PartialEq)]
pub struct Hash {
    pub value: u64,
}

impl Hash {
    pub fn new(value: u64) -> Self {
        Self { value }
    }
}

#[macro_export]
macro_rules! hash {
    ($lit:literal) => {{
        $crate::hash::Hash::new(habanero_macros::string_hash!($lit))
    }};
}
