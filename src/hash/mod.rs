pub use self::string_hash::*;

#[macro_use]
mod string_hash;

pub trait TypeHash {
    fn type_hash() -> Hash;
}
