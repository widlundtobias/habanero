use anyhow::anyhow;
use dpx_db::table::Table;
/// defines a region on the screen that can be rendered to
#[derive(Debug, Clone, Copy)]
pub struct Viewport {
    pub start: glm::UVec2,
    pub size: glm::UVec2,
    pub clear_color: crate::color::Color,
}

impl Viewport {
    pub fn apply(&self, gl: &crate::gl::Gl) {
        unsafe {
            gl.Viewport(
                self.start.x as i32,
                self.start.y as i32,
                self.size.x as i32,
                self.size.y as i32,
            );
        }
    }
}

pub fn adjust_viewports_with(f: impl Fn(&mut Viewport, usize, usize), t: &mut super::TViewport) {
    let count = t.len();

    for (i, v) in t.iter_mut().enumerate() {
        f(v.data, i, count);
    }
}

/// singe-viewport function. assumes there's just one viewport
pub fn adjust_single_viewport_start_size(
    start: glm::UVec2,
    size: glm::UVec2,
    t: &mut super::TViewport,
) -> anyhow::Result<()> {
    if t.len() > 1 {
        return Err(anyhow!(
            "Cannot use single viewport function with more than one viewport"
        ));
    }

    let v = t
        .first_mut()
        .ok_or(anyhow!(
            "Cannot use single viewport function with no viewport"
        ))?
        .data;

    v.start = start;
    v.size = size;

    Ok(())
}
