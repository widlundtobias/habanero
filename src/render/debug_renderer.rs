use super::*;
use dpx_db::table::Table;

type DynPrimitiveList = DynamicPrimitiveList<DefaultVertexStorage>;
pub trait DebugRender {
    fn to_primitives(&self, options: &DebugDrawableOptions) -> DynPrimitiveList;
    fn set_depth_2d(&mut self, depth: f32);
}

pub struct DebugDrawableOptions {
    ttl: u32,
    color: crate::color::Color,
}

impl DebugDrawableOptions {
    pub fn new() -> Self {
        Self {
            ttl: 1,
            color: crate::color::white(),
        }
    }
    pub fn set_ttl(&mut self, ttl: u32) {
        self.ttl = ttl
    }
    pub fn set_color(&mut self, col: crate::color::Color) {
        self.color = col
    }
}

pub struct DebugRendererDispatcher {
    pub shader: ShaderId,
}

impl RendererDispatcher for DebugRendererDispatcher {
    fn dispatch_render(
        &self,
        settings: &RenderSettings,
        viewport: &Viewport,
        camera: &Camera,
        renderer: &dyn Renderer,
        texture_t: &TTexture,
        shader_t: &TShader,
        uniform_location_t: &mut TUniformLocation,
        gl: &crate::gl::Gl,
    ) -> anyhow::Result<()> {
        renderer
            .as_any()
            .downcast_ref::<DebugRenderer>()
            .unwrap()
            .render(
                settings,
                viewport,
                camera,
                shader_t.get(self.shader).unwrap(),
                texture_t,
                uniform_location_t,
                gl,
            )?;

        Ok(())
    }
}

#[derive(Debug)]
pub enum DebugRenderError {
    NoTexture(TextureId),
}

impl std::error::Error for DebugRenderError {}
impl std::fmt::Display for DebugRenderError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            DebugRenderError::NoTexture(id) => write!(
                f,
                "The blank texture id {} given for the debug renderer is invalid.",
                id
            ),
        }
    }
}
pub struct DebugRenderer {
    vao: crate::gl::Vao,
    positions_vbo: crate::gl::ArrayVbo,
    tex_coords_vbo: crate::gl::ArrayVbo,
    colors_vbo: crate::gl::ArrayVbo,
}

impl DebugRenderer {
    pub fn new(gl: std::rc::Rc<crate::gl::Gl>) -> Self {
        let positions_vbo = crate::gl::ArrayVbo::new(gl.clone());
        let tex_coords_vbo = crate::gl::ArrayVbo::new(gl.clone());
        let colors_vbo = crate::gl::ArrayVbo::new(gl.clone());

        let vao = crate::gl::Vao::new(
            &vec![
                crate::gl::VertexAttribPointerDefinition::new(
                    &positions_vbo,
                    0,
                    <DefaultVertexStorage as VertexAttributeStorage<PositionXYZTag>>::definition(),
                ),
                crate::gl::VertexAttribPointerDefinition::new(
                    &tex_coords_vbo,
                    1,
                    <DefaultVertexStorage as VertexAttributeStorage<TextureUVTag>>::definition(),
                ),
                crate::gl::VertexAttribPointerDefinition::new(
                    &colors_vbo,
                    2,
                    <DefaultVertexStorage as VertexAttributeStorage<ColorRGBATag>>::definition(),
                ),
            ],
            gl,
        );

        Self {
            vao,
            positions_vbo,
            tex_coords_vbo,
            colors_vbo,
        }
    }
    pub fn tick_time() {}

    pub fn render(
        &self, //static or not? it's static in C++
        settings: &RenderSettings,
        viewport: &Viewport,
        camera: &Camera,
        shader: &crate::gl::Shader, //unsure
        texture_t: &TTexture,
        uniform_location_t: &mut TUniformLocation,
        gl: &crate::gl::Gl,
    ) -> anyhow::Result<()> {
        let texture = texture_t
            .get(settings.blank_texture)
            .ok_or(DebugRenderError::NoTexture(settings.blank_texture))?;

        let to_render = GlobalData::obtain();

        self.vao.bind(gl);

        viewport.apply(&gl);

        shader.bind(&gl);

        //calculate projection matrix from camera and set the shader uniform for it
        let vp_mat = camera.view_projection_matrix();
        let projection_matrix_location = UniformLocation::cached_get(
            &shader,
            default_uniforms::VIEW_PROJECTION_UNIFORM,
            uniform_location_t,
            &gl,
        );

        unsafe {
            gl.UniformMatrix4fv(
                projection_matrix_location.location,
                1,
                crate::gl::FALSE,
                vp_mat.as_ptr(),
            );
        }

        let texture_location = UniformLocation::cached_get(
            &shader,
            default_uniforms::TEXTURE_UNIFORM,
            uniform_location_t,
            &gl,
        );
        texture.bind(crate::gl::TEXTURE_2D, 0, &gl);
        unsafe {
            gl.Uniform1i(texture_location.location, 0);
        }

        for GlobalDataEntry { drawable: list, .. } in to_render {
            let upload_data = vec![
                VertexUploadData::new::<PositionXYZTag, _>(
                    &self.positions_vbo,
                    list.vertices(),
                    0,
                    list.vertices().vertex_count(),
                ),
                VertexUploadData::new::<TextureUVTag, _>(
                    &self.tex_coords_vbo,
                    list.vertices(),
                    0,
                    list.vertices().vertex_count(),
                ),
                VertexUploadData::new::<ColorRGBATag, _>(
                    &self.colors_vbo,
                    list.vertices(),
                    0,
                    list.vertices().vertex_count(),
                ),
            ];

            super::primitives::draw_primitives_vbo(
                &list,
                &upload_data,
                crate::gl::STREAM_DRAW,
                list.vertices().vertex_count(),
                gl,
            );
        }

        crate::gl::Vao::unbind(gl);

        Ok(())
    }
}

impl Renderer for DebugRenderer {
    fn frame_start(&self) {}
}

////the global data that the debug renderer relies on

use lazy_static::lazy_static;
use std::sync::Mutex;

lazy_static! {
    //I think this can be made faster with a lock-free queue or something but whatever
    static ref INSTANCE: Mutex<GlobalData> = Mutex::new(GlobalData {
        drawables: Vec::new()
    });
}

pub fn push_debug_render(drawable: &impl DebugRender, options: &DebugDrawableOptions) {
    GlobalData::push(drawable, options)
}

struct GlobalDataEntry {
    ttl: u32,
    drawable: DynPrimitiveList,
}
struct GlobalData {
    drawables: Vec<GlobalDataEntry>,
}

impl GlobalData {
    fn obtain() -> Vec<GlobalDataEntry> {
        let old_drawables = &mut INSTANCE.lock().unwrap().drawables; //poison error unwrapped
        let mut new_drawables: Vec<GlobalDataEntry> = old_drawables
            .iter()
            .filter_map(|e| {
                if e.ttl > 1 {
                    return Some(GlobalDataEntry {
                        ttl: e.ttl - 1,
                        drawable: e.drawable.clone(),
                    });
                } else {
                    return None;
                }
            })
            .collect();

        std::mem::swap(&mut new_drawables, old_drawables);

        new_drawables
    }

    fn push(drawable: &impl DebugRender, options: &DebugDrawableOptions) {
        INSTANCE.lock().unwrap().drawables.push(GlobalDataEntry {
            ttl: options.ttl,
            drawable: drawable.to_primitives(options),
        })
    }
}

#[macro_export]
macro_rules! dren {
    (@inner :ttl: $e:expr, $t:ident, $d:ident) => {
        $d.set_ttl($e);
    };
    (@inner :color: $e:expr, $t:ident, $d:ident) => {
        $d.set_color($e);
    };
    (@inner :depth: $e:expr, $t:ident, $d:ident) => {
        $t.set_depth_2d($e);
    };
    (@inner :$_invalid:ident: $t:expr, $d:ident) => {
        let error: () = "Error: invalid setting";
    };


    ($(:$v:ident: $b:expr,)* $($t:expr,)+) => {
        {
            let mut d = $crate::render::DebugDrawableOptions::new();

            fn apply_options<T: $crate::render::DebugRender> (t: T, d: &mut $crate::render::DebugDrawableOptions) -> T {
                let mut t = t;

                $(dren!(@inner :$v: $b, t, d);)*

                t
            };

            $($crate::render::push_debug_render(&apply_options($t, &mut d), &d);)+
        }
    }
}

pub struct DRect {
    pub start: glm::Vec3,
    pub size: glm::Vec3,
    //pub color: crate::color::Color,
}

impl DRect {
    pub fn new_2d(start: glm::Vec2, size: glm::Vec2) -> DRect {
        DRect {
            start: glm::vec3(start.x, start.y, 0.0),
            size: glm::vec3(size.x, size.y, 0.0),
        }
    }
}

impl DebugRender for DRect {
    fn to_primitives(&self, options: &DebugDrawableOptions) -> DynPrimitiveList {
        let mut res =
            DynPrimitiveList::new_with_vertex_count(primitives::PrimitiveType::Triangles, 6);
        let storage = res.vertices_mut();

        let vertices = quad_2d(
            //Not sure how to support 3D here
            self.start.xy(),
            self.size.xy(),
            self.start.z,
            glm::zero(),
            glm::vec2(1.0, 1.0),
            options.color,
        );

        DefaultVertex::write_to(vertices.iter(), storage, 0);

        res
    }
    fn set_depth_2d(&mut self, depth: f32) {
        self.start.z = depth;
        self.size.z = 0.0;
    }
}

pub struct DArrow {
    pub start: glm::Vec3,
    pub end: glm::Vec3,
    pub head_length: f32,
}

impl DArrow {
    pub fn new_2d(start: glm::Vec2, end: glm::Vec2, head_length: f32) -> DArrow {
        DArrow {
            start: glm::vec3(start.x, start.y, 0.0),
            end: glm::vec3(end.x, end.y, 0.0),
            head_length,
        }
    }
}
impl DebugRender for DArrow {
    fn to_primitives(&self, options: &DebugDrawableOptions) -> DynPrimitiveList {
        let mut res = DynPrimitiveList::new_with_vertex_count(primitives::PrimitiveType::Lines, 6);
        let storage = res.vertices_mut();

        let depth = self.start.z;

        //main line
        let vertices = line_2d(
            //Not sure how to support 3D here
            self.start.xy(),
            self.end.xy(),
            depth,
            glm::zero(),
            glm::vec2(1.0, 1.0),
            options.color,
        );
        DefaultVertex::write_to(vertices.iter(), storage, 0);

        //head lines
        let angle = glm::pi::<f32>() / 4.0;
        let half_angle = angle / 2.0;
        let reverse_direction = glm::normalize(&(self.start - self.end).xy());

        //positive head line
        let positive_head_dir = glm::rotate_vec2(&reverse_direction, half_angle);
        let positive_head_vec = positive_head_dir * self.head_length;
        let positive_head_end =
            self.end + glm::vec3(positive_head_vec.x, positive_head_vec.y, self.start.z);

        let vertices = line_2d(
            positive_head_end.xy(),
            self.end.xy(),
            depth,
            glm::zero(),
            glm::vec2(1.0, 1.0),
            options.color,
        );

        DefaultVertex::write_to(vertices.iter(), storage, 2);

        //negative head line
        let negative_head_dir = glm::rotate_vec2(&reverse_direction, -half_angle);
        let negative_head_vec = negative_head_dir * self.head_length;
        let negative_head_end =
            self.end + glm::vec3(negative_head_vec.x, negative_head_vec.y, self.start.z);

        let vertices = line_2d(
            negative_head_end.xy(),
            self.end.xy(),
            depth,
            glm::zero(),
            glm::vec2(1.0, 1.0),
            options.color,
        );
        DefaultVertex::write_to(vertices.iter(), storage, 4);

        res
    }
    fn set_depth_2d(&mut self, depth: f32) {
        self.start.z = depth;
        self.end.z = depth;
    }
}
