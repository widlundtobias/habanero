use super::*;
use dpx_db::table::*;
use std::collections::HashMap;

/// Shapes the behavior of rendering
pub struct RenderSettings {
    /// available as a fallback for renderers that are given stuff to render that lacks a texture.
    pub default_texture: Option<TextureId>,
    /// needed by renderers that render pure color based stuff like the debug renderer
    pub blank_texture: TextureId,
    pub clear_viewports: bool,
}

impl RenderSettings {
    pub fn new(texture_t: &mut TTexture, gl: std::rc::Rc<crate::gl::Gl>) -> Self {
        let config = crate::gl::TextureConfiguration {
            filter_mode: crate::gl::FilterMode::Nearest,
            generate_mipmaps: false,
            wrap_mode: crate::gl::WrapMode::Clamp,
        };

        let white_texture =
            crate::gl::Texture::new_rgba_from_color(crate::color::white(), &config, gl);
        let white_texture = texture_t.insert(white_texture).id;
        Self {
            default_texture: Some(white_texture),
            blank_texture: white_texture,
            clear_viewports: true,
        }
    }
}

pub fn frame_start(render_core_tm: &CoreModule) {
    for renderer in &render_core_tm.renderer_t {
        renderer.data.frame_start();
    }
}

pub fn render_frame(
    dispatchers: HashMap<RendererId, &dyn RendererDispatcher>,
    settings: &RenderSettings,
    render_pass_t: &TRenderPass,
    renderer_t: &TRenderer,
    viewport_t: &TViewport,
    camera_t: &TCamera,
    texture_t: &TTexture,
    shader_t: &TShader,
    uniform_location_t: &mut TUniformLocation,
    gl: &crate::gl::Gl,
) -> anyhow::Result<()> {
    ////clear viewports
    if settings.clear_viewports {
        unsafe {
            gl.Enable(crate::gl::SCISSOR_TEST);
        }
        for dpx_db::table::TableEntry { data: vp, .. } in viewport_t {
            unsafe {
                gl.Scissor(
                    vp.start.x as i32,
                    vp.start.y as i32,
                    vp.size.x as i32,
                    vp.size.y as i32,
                );
                let clear_color = crate::color::NColor::from(vp.clear_color);
                gl.ClearColor(clear_color.r, clear_color.g, clear_color.b, clear_color.a);

                gl.Clear(crate::gl::DEPTH_BUFFER_BIT | crate::gl::COLOR_BUFFER_BIT);
            }
        }
        unsafe {
            gl.Disable(crate::gl::SCISSOR_TEST);
        }
    }

    ////render
    let mut render_pass_by_viewport: HashMap<ViewportId, Vec<&RenderPass>> = HashMap::new();

    //collect all render passes by which viewports they apply to
    for TableEntry {
        data: render_pass, ..
    } in render_pass_t
    {
        render_pass_by_viewport
            .entry(render_pass.viewport)
            .or_insert(Vec::new())
            .push(&render_pass);
    }

    //sort the collections by the render pass order
    for (_, passes) in &mut render_pass_by_viewport {
        passes.sort_by(|&a, &b| a.order.cmp(&b.order));
    }

    //now perform the rendering of all render passes
    for (_, passes) in &mut render_pass_by_viewport {
        //set viewport and clear
        for pass in passes {
            let renderer_id = pass.renderer;
            let renderer = renderer_t.get(renderer_id).unwrap();

            let viewport = viewport_t.get(pass.viewport).unwrap();
            let camera = camera_t.get(pass.camera).unwrap();
            let dispatcher = *dispatchers.get(&renderer_id).unwrap();
            dispatcher.dispatch_render(
                &settings,
                viewport,
                camera,
                renderer.as_ref(),
                texture_t,
                shader_t,
                uniform_location_t,
                &gl,
            )?;
            //renderer.render(viewport, camera, subject_providers.remove(&renderer_id));
        }
    }

    Ok(())
}

pub fn add_renderer<T: Renderer + 'static>(
    id: RendererId,
    table: &mut TRenderer,
    r: T,
) -> TableEntry<RendererId, &mut Box<dyn Renderer>> {
    match table.insert(id, Box::new(r)) {
        WriteResult::Written(e) => e,
        _ => panic!(format!("renderer with id {} already exists!", id)),
    }
}
