use super::*;
use dpx_db::table::Table;
use std::collections::HashMap;

use boolinator::Boolinator;

#[derive(Copy, Clone)]
pub struct Quad<'a> {
    //batch parameters
    pub texture: Option<&'a crate::gl::Texture>,
    pub shader: &'a crate::gl::Shader,
    //affects sorting but not batch
    pub z: f32,
    //the rest
    pub size: glm::Vec2,
    pub position: glm::Vec2,
    pub rotation: f32,
    pub color: crate::color::Color,
    pub texture_start: glm::Vec2,
    pub texture_end: glm::Vec2,
}

impl<'a> Quad<'a> {
    pub fn new_with_shader(shader: &'a crate::gl::Shader) -> Self {
        Self {
            shader,
            texture: Default::default(),
            z: Default::default(),
            size: Default::default(),
            position: Default::default(),
            rotation: Default::default(),
            color: Default::default(),
            texture_start: Default::default(),
            texture_end: Default::default(),
        }
    }

    fn vertex_count() -> u32 {
        6
    }
}

pub trait QuadBuilderDispatcher<'a> {
    fn dispatch_generate_quads(
        &self,
        target: &mut Vec<Quad<'a>>,
        camera: &Camera,
        builder: &dyn QuadBuilder,
    );
}

pub trait QuadBuilder: as_any::AsAny {
    fn unique_id(&self) -> u32;
}

pub struct QuadRendererDispatcher<'a> {
    pub builder_dispatchers: &'a HashMap<u32, &'a dyn QuadBuilderDispatcher<'a>>,
}

impl<'a> RendererDispatcher for QuadRendererDispatcher<'a> {
    fn dispatch_render(
        &self,
        settings: &RenderSettings,
        viewport: &Viewport,
        camera: &Camera,
        renderer: &dyn Renderer,
        texture_t: &TTexture,
        _: &TShader,
        uniform_location_t: &mut TUniformLocation,
        gl: &crate::gl::Gl,
    ) -> anyhow::Result<()> {
        renderer
            .as_any()
            .downcast_ref::<QuadRenderer>()
            .unwrap()
            .render(
                settings,
                viewport,
                camera,
                &self.builder_dispatchers,
                texture_t,
                uniform_location_t,
                gl,
            )?;

        Ok(())
    }
}

pub struct QuadRenderer {
    //cache: triangles: TrianglesPrimitiveList<DefaultVertexStorage>,
    quad_builders: Vec<Box<dyn QuadBuilder>>,
    vao: crate::gl::Vao,
    positions_vbo: crate::gl::ArrayVbo,
    tex_coords_vbo: crate::gl::ArrayVbo,
    colors_vbo: crate::gl::ArrayVbo,
}

#[derive(Debug)]
pub enum QuadRenderError {
    NoTexture,
}

impl std::error::Error for QuadRenderError {}
impl std::fmt::Display for QuadRenderError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            QuadRenderError::NoTexture => write!(
                f,
                "There was a quad that lacked a texture and there was no default texture given"
            ),
        }
    }
}

impl QuadRenderer {
    pub fn new(gl: std::rc::Rc<crate::gl::Gl>) -> Self {
        let positions_vbo = crate::gl::ArrayVbo::new(gl.clone());
        let tex_coords_vbo = crate::gl::ArrayVbo::new(gl.clone());
        let colors_vbo = crate::gl::ArrayVbo::new(gl.clone());

        let vao = crate::gl::Vao::new(
            &vec![
                crate::gl::VertexAttribPointerDefinition::new(
                    &positions_vbo,
                    0,
                    <DefaultVertexStorage as VertexAttributeStorage<PositionXYZTag>>::definition(),
                ),
                crate::gl::VertexAttribPointerDefinition::new(
                    &tex_coords_vbo,
                    1,
                    <DefaultVertexStorage as VertexAttributeStorage<TextureUVTag>>::definition(),
                ),
                crate::gl::VertexAttribPointerDefinition::new(
                    &colors_vbo,
                    2,
                    <DefaultVertexStorage as VertexAttributeStorage<ColorRGBATag>>::definition(),
                ),
            ],
            gl,
        );

        Self {
            quad_builders: Vec::new(),
            vao,
            positions_vbo,
            tex_coords_vbo,
            colors_vbo,
        }
    }
    pub fn new_with_builders(
        builders: Vec<Box<dyn QuadBuilder>>,
        gl: std::rc::Rc<crate::gl::Gl>,
    ) -> Self {
        let mut res = QuadRenderer::new(gl);
        res.quad_builders = builders;
        res
    }
    pub fn add_builder<T: QuadBuilder + 'static>(&mut self, builder: T) {
        self.quad_builders.push(Box::new(builder))
    }

    fn render(
        &self,
        settings: &RenderSettings,
        viewport: &Viewport,
        camera: &Camera,
        dispatchers: &HashMap<u32, &dyn QuadBuilderDispatcher>,
        texture_t: &TTexture,
        uniform_location_t: &mut TUniformLocation,
        gl: &crate::gl::Gl,
    ) -> Result<(), QuadRenderError> {
        let mut quads = Vec::<Quad>::new();

        for builder in &self.quad_builders {
            let dispatcher = *dispatchers.get(&builder.unique_id()).unwrap();
            dispatcher.dispatch_generate_quads(&mut quads, camera, builder.as_ref());
        }

        if quads.is_empty() {
            return Ok(());
        }

        quads.sort_by(|a, b| {
            (
                a.z,
                a.texture.map(|t| t.gl_handle),
                a.shader.program_gl_handle, //assuming it is enough to compare just the program handle
            )
                .partial_cmp(&(
                    b.z,
                    b.texture.map(|t| t.gl_handle),
                    b.shader.program_gl_handle,
                ))
                .unwrap()
        });

        let mut triangles = TrianglesPrimitiveList::<DefaultVertexStorage>::new_with_vertex_count(
            quads.len() * Quad::vertex_count() as usize,
        );

        self.vao.bind(gl);

        viewport.apply(gl);

        //calculate projection matrix from camera and set the shader uniform for it
        let vp_mat = camera.view_projection_matrix();

        let mut batches = Vec::<BatchInfo>::new();
        let mut last_quad_texture: Option<&crate::gl::Texture> = None;
        let mut last_quad_shader: Option<&crate::gl::Shader> = None;

        for (i, quad) in quads.iter().enumerate() {
            let quad_vertex_start = i * Quad::vertex_count() as usize;
            let quad_vertex_range =
                quad_vertex_start..(quad_vertex_start + Quad::vertex_count() as usize);

            //// generate vertices from quad and write to the vertex storage
            //positions
            let size = quad.size;
            let half_size = size / 2.0;
            let position = quad.position - half_size;
            let rotate_and_position_vertex = |vertex: glm::Vec2| {
                let mut vertex = vertex;
                vertex -= half_size;
                vertex = glm::rotate_vec2(&vertex, -quad.rotation);
                vertex += position + half_size;
                vertex
            };

            let add_z = |v: glm::Vec2| glm::vec3(v.x, v.y, quad.z);
            let tl = add_z(rotate_and_position_vertex(glm::vec2(0.0, 0.0)));
            let tr = add_z(rotate_and_position_vertex(glm::vec2(size.x, 0.0)));
            let bl = add_z(rotate_and_position_vertex(glm::vec2(0.0, size.y)));
            let br = add_z(rotate_and_position_vertex(glm::vec2(size.x, size.y)));

            let camera_facing = false; //this should be a setting
            if camera_facing {
                todo!();
                /*
                ZoneScopedN("rotate to cam")
                glm::vec3 spritePos = glm::vec3(currentQuad.position, z);

                glm::mat4 sprite3dOrientation = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), camera.translation - spritePos, glm::vec3(0.0f, 1.0f, 0.0f));

                tl -= spritePos;
                tr -= spritePos;
                bl -= spritePos;
                br -= spritePos;
                tl = swizzle("xyz", glm::vec4(tl, 1.0f) * sprite3dOrientation);
                tr = swizzle("xyz", glm::vec4(tr, 1.0f) * sprite3dOrientation);
                bl = swizzle("xyz", glm::vec4(bl, 1.0f) * sprite3dOrientation);
                br = swizzle("xyz", glm::vec4(br, 1.0f) * sprite3dOrientation);
                tl += spritePos;
                tr += spritePos;
                bl += spritePos;
                br += spritePos;*/
            }

            let to_add = [tl, bl, tr, tr, bl, br];
            //write to positions
            let write_target = <_ as VertexAttributeStorage<PositionXYZTag>>::get_slice_mut(
                triangles.vertices_mut(),
                quad_vertex_range.clone(),
            );
            write_target.copy_from_slice(&to_add);

            //texture coordinates
            let tx_start = quad.texture_start;
            let tx_end = quad.texture_end;
            let to_add = [
                glm::vec2(tx_start.x, tx_start.y),
                glm::vec2(tx_start.x, tx_end.y),
                glm::vec2(tx_end.x, tx_start.y),
                glm::vec2(tx_end.x, tx_start.y),
                glm::vec2(tx_start.x, tx_end.y),
                glm::vec2(tx_end.x, tx_end.y),
            ];
            //write to tex coords
            let write_target = <_ as VertexAttributeStorage<TextureUVTag>>::get_slice_mut(
                triangles.vertices_mut(),
                quad_vertex_range.clone(),
            );
            write_target.copy_from_slice(&to_add);

            //colors
            let color = quad.color.to_vec();
            //write to colors
            let write_target = <_ as VertexAttributeStorage<ColorRGBATag>>::get_slice_mut(
                triangles.vertices_mut(),
                quad_vertex_range.clone(),
            );
            for c in write_target {
                *c = color;
            }

            //use default texture if it's not set
            let default_texture = texture_t.map_to_data(settings.default_texture);
            let texture = quad
                .texture
                .or(default_texture)
                .ok_or(QuadRenderError::NoTexture)?;

            //// calculate batch information

            //determine if this quad belongs in a new render batch or not
            let is_same_batch = match (last_quad_texture, last_quad_shader) {
                (Some(t), Some(s)) => {
                    t.gl_handle == texture.gl_handle
                        && s.program_gl_handle == quad.shader.program_gl_handle //assuming it is enough to compare just program handle
                }
                _ => false,
            };

            let last_batch = batches.last_mut();
            if is_same_batch {
                //add quad to existing batch vertex range
                last_batch.unwrap().vertex_range.end += Quad::vertex_count() as usize;
            } else {
                //this marks a new batch. the initial range of this batch is the same as the range
                //of this quad since it's the only one in the batch at first

                //either create the first batch, or a subsequent batch.
                //note down the info if there is a change in something significant for this batch
                let new_batch = match last_batch {
                    None => BatchInfo {
                        texture_change: Some(texture),
                        shader_change: Some(quad.shader),
                        vertex_range: quad_vertex_range.clone(),
                    },
                    Some(_) => BatchInfo {
                        texture_change: (texture.gl_handle != last_quad_texture.unwrap().gl_handle)
                            .as_some(texture),
                        shader_change: (quad.shader.program_gl_handle
                            != last_quad_shader.unwrap().program_gl_handle) //assuming it is enough to compare just program handle
                            .as_some(quad.shader),
                        vertex_range: quad_vertex_range.clone(),
                    },
                };
                batches.push(new_batch);
            }

            //for the next iteration
            last_quad_texture = Some(texture);
            last_quad_shader = Some(quad.shader);
        }

        //// now render all batches
        if !batches.is_empty() {
            let mut current_shader = batches.first().unwrap().shader_change.unwrap(); //both options known to be unwrappable

            for batch in batches {
                if let Some(shader) = batch.shader_change {
                    current_shader = shader;
                    shader.bind(&gl);
                    let projection_matrix_location = UniformLocation::cached_get(
                        &current_shader,
                        default_uniforms::VIEW_PROJECTION_UNIFORM,
                        uniform_location_t,
                        &gl,
                    );
                    unsafe {
                        gl.UniformMatrix4fv(
                            projection_matrix_location.location,
                            1,
                            crate::gl::FALSE,
                            vp_mat.as_ptr(),
                        );
                    }
                }
                if let Some(texture) = batch.texture_change {
                    let texture_location = UniformLocation::cached_get(
                        &current_shader,
                        default_uniforms::TEXTURE_UNIFORM,
                        uniform_location_t,
                        &gl,
                    );
                    texture.bind(crate::gl::TEXTURE_2D, 0, &gl);
                    unsafe {
                        gl.Uniform1i(texture_location.location, 0);
                    }
                }

                let start = batch.vertex_range.start;
                let count = batch.vertex_range.len();

                let upload_data = vec![
                    VertexUploadData::new::<PositionXYZTag, _>(
                        &self.positions_vbo,
                        triangles.vertices(),
                        start,
                        count,
                    ),
                    VertexUploadData::new::<TextureUVTag, _>(
                        &self.tex_coords_vbo,
                        triangles.vertices(),
                        start,
                        count,
                    ),
                    VertexUploadData::new::<ColorRGBATag, _>(
                        &self.colors_vbo,
                        triangles.vertices(),
                        start,
                        count,
                    ),
                ];

                super::primitives::draw_primitives_vbo(
                    &triangles,
                    &upload_data,
                    crate::gl::STREAM_DRAW,
                    count,
                    gl,
                )
            }
        }

        crate::gl::Vao::unbind(&gl);
        Ok(())
    }
}

struct BatchInfo<'a> {
    texture_change: Option<&'a crate::gl::Texture>,
    shader_change: Option<&'a crate::gl::Shader>,
    vertex_range: std::ops::Range<usize>,
}

impl Renderer for QuadRenderer {
    fn frame_start(&self) {}
}
