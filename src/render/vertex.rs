pub trait VertexStorage: Clone {
    fn new() -> Self;
    fn new_with_count(size: usize) -> Self;
    fn vertex_count(&self) -> usize;
}
pub trait VertexAttributeTag {
    type AttributeType;
    fn attribute_index() -> u32;
}
pub trait VertexAttributeStorage<T: VertexAttributeTag> {
    fn get_slice(&self, vertex_range: std::ops::Range<usize>) -> &[T::AttributeType];
    fn get_slice_mut(&mut self, vertex_range: std::ops::Range<usize>) -> &mut [T::AttributeType];
    fn get_slice_raw<'a>(&'a self, vertex_range: std::ops::Range<usize>) -> &'a [u8] {
        let a = self.get_slice(vertex_range);
        unsafe {
            std::slice::from_raw_parts(
                a.as_ptr() as *const u8,
                a.len() * std::mem::size_of::<T::AttributeType>(),
            )
        }
    }

    fn definition() -> VertexAttributeDefinition;
}

pub struct VertexAttributeDefinition {
    pub dimension_count: u8,
    pub data_type: u32, //what is this??????
    pub normalized: bool,
    pub stride: u32,
    pub offset: usize,
}
