use super::*;
use as_any::AsAny;

pub trait RendererDispatcher {
    fn dispatch_render(
        &self,
        settings: &RenderSettings,
        viewport: &Viewport,
        camera: &Camera,
        renderer: &dyn Renderer,
        texture_t: &TTexture,
        shader_t: &TShader,
        uniform_location_t: &mut TUniformLocation,
        gl: &crate::gl::Gl,
    ) -> anyhow::Result<()>;
}
/// can render
pub trait Renderer: AsAny {
    fn frame_start(&self);
}
