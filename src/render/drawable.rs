use super::*;
use crate::hash::*;

#[derive(Copy, Clone)]
pub struct DrawableShader {
    pub shader: db::ShaderId,
}

impl TypeHash for DrawableShader {
    fn type_hash() -> crate::hash::Hash {
        hash!("DrawableShader")
    }
}

#[derive(Copy, Clone)]
pub struct DrawableColor {
    pub color: crate::color::Color,
}

impl TypeHash for DrawableColor {
    fn type_hash() -> crate::hash::Hash {
        hash!("DrawableColor")
    }
}
