use super::*;
use itertools::izip;
pub struct PositionXYZTag {}
impl VertexAttributeTag for PositionXYZTag {
    type AttributeType = glm::Vec3;
    fn attribute_index() -> u32 {
        0
    }
}
pub struct TextureUVTag {}
impl VertexAttributeTag for TextureUVTag {
    type AttributeType = glm::Vec2;
    fn attribute_index() -> u32 {
        1
    }
}
pub struct ColorRGBATag {}
impl VertexAttributeTag for ColorRGBATag {
    type AttributeType = glm::U8Vec4;
    fn attribute_index() -> u32 {
        2
    }
}

#[derive(Clone)]
pub struct DefaultVertexStorage {
    pub position_xyz: Vec<<PositionXYZTag as VertexAttributeTag>::AttributeType>,
    pub texture_uv: Vec<<TextureUVTag as VertexAttributeTag>::AttributeType>,
    pub color_rgba: Vec<<ColorRGBATag as VertexAttributeTag>::AttributeType>,
}
impl VertexStorage for DefaultVertexStorage {
    fn new() -> Self {
        Self {
            position_xyz: Vec::new(),
            texture_uv: Vec::new(),
            color_rgba: Vec::new(),
        }
    }
    fn new_with_count(size: usize) -> Self {
        Self {
            position_xyz: vec![glm::zero(); size],
            texture_uv: vec![glm::zero(); size],
            color_rgba: vec![glm::zero(); size],
        }
    }
    fn vertex_count(&self) -> usize {
        self.position_xyz.len()
    }
}
impl VertexAttributeStorage<PositionXYZTag> for DefaultVertexStorage {
    fn get_slice(
        &self,
        range: std::ops::Range<usize>,
    ) -> &[<PositionXYZTag as VertexAttributeTag>::AttributeType] {
        &self.position_xyz[range]
    }
    fn get_slice_mut(
        &mut self,
        range: std::ops::Range<usize>,
    ) -> &mut [<PositionXYZTag as VertexAttributeTag>::AttributeType] {
        &mut self.position_xyz[range]
    }
    fn definition() -> VertexAttributeDefinition {
        VertexAttributeDefinition {
            dimension_count: 3,
            data_type: crate::gl::FLOAT,
            normalized: false,
            stride: 0,
            offset: 0,
        }
    }
}
impl VertexAttributeStorage<TextureUVTag> for DefaultVertexStorage {
    fn get_slice(
        &self,
        range: std::ops::Range<usize>,
    ) -> &[<TextureUVTag as VertexAttributeTag>::AttributeType] {
        &self.texture_uv[range]
    }
    fn get_slice_mut(
        &mut self,
        range: std::ops::Range<usize>,
    ) -> &mut [<TextureUVTag as VertexAttributeTag>::AttributeType] {
        &mut self.texture_uv[range]
    }
    fn definition() -> VertexAttributeDefinition {
        VertexAttributeDefinition {
            dimension_count: 2,
            data_type: crate::gl::FLOAT,
            normalized: false,
            stride: 0,
            offset: 0,
        }
    }
}

impl VertexAttributeStorage<ColorRGBATag> for DefaultVertexStorage {
    fn get_slice(
        &self,
        range: std::ops::Range<usize>,
    ) -> &[<ColorRGBATag as VertexAttributeTag>::AttributeType] {
        &self.color_rgba[range]
    }
    fn get_slice_mut(
        &mut self,
        range: std::ops::Range<usize>,
    ) -> &mut [<ColorRGBATag as VertexAttributeTag>::AttributeType] {
        &mut self.color_rgba[range]
    }

    fn definition() -> VertexAttributeDefinition {
        VertexAttributeDefinition {
            dimension_count: 4,
            data_type: crate::gl::UNSIGNED_BYTE,
            normalized: true,
            stride: 0,
            offset: 0,
        }
    }
}

//this might be incorporated with the rest through traits
#[derive(Copy, Clone)]
pub struct DefaultVertex {
    pub position: glm::Vec3,
    pub uv: glm::Vec2,
    pub color: glm::U8Vec4,
}

impl DefaultVertex {
    pub fn write_to<'a>(
        iter: impl Iterator<Item = &'a DefaultVertex>,
        storage: &mut DefaultVertexStorage,
        start_index: usize,
    ) {
        let vertex_iterator = izip!(
            storage.position_xyz.iter_mut().skip(start_index),
            storage.texture_uv.iter_mut().skip(start_index),
            storage.color_rgba.iter_mut().skip(start_index),
            iter
        );

        for (pos, uv, col, vert) in vertex_iterator {
            *pos = vert.position;
            *uv = vert.uv;
            *col = vert.color;
        }
    }
}

//// Geometry creation helpers

pub fn quad_2d(
    start: glm::Vec2,
    size: glm::Vec2,
    depth: f32,
    uv_min: glm::Vec2,
    uv_max: glm::Vec2,
    col: crate::color::Color,
) -> [DefaultVertex; 6] {
    let mut res: [DefaultVertex; 6] = [DefaultVertex {
        position: glm::zero(),
        uv: glm::zero(),
        color: col.to_vec(),
    }; 6];

    let p_tl = glm::vec3(start.x, start.y, depth);
    let uv_tl = glm::vec2(uv_min.x, uv_min.y);
    let p_tr = glm::vec3(start.x + size.x, start.y, depth);
    let uv_tr = glm::vec2(uv_max.x, uv_min.y);
    let p_bl = glm::vec3(start.x, start.y + size.y, depth);
    let uv_bl = glm::vec2(uv_min.x, uv_max.y);
    let p_br = glm::vec3(start.x + size.x, start.y + size.y, depth);
    let uv_br = glm::vec2(uv_max.x, uv_max.y);

    res[0].position = p_tl;
    res[0].uv = uv_tl;
    res[1].position = p_bl;
    res[1].uv = uv_bl;
    res[2].position = p_br;
    res[2].uv = uv_br;

    res[3].position = p_br;
    res[3].uv = uv_br;
    res[4].position = p_tr;
    res[4].uv = uv_tr;
    res[5].position = p_tl;
    res[5].uv = uv_tl;

    res
}

pub fn line_2d(
    start: glm::Vec2,
    end: glm::Vec2,
    depth: f32,
    uv_min: glm::Vec2,
    uv_max: glm::Vec2,
    col: crate::color::Color,
) -> [DefaultVertex; 2] {
    [
        DefaultVertex {
            position: glm::vec3(start.x, start.y, depth),
            uv: uv_min,
            color: col.to_vec(),
        },
        DefaultVertex {
            position: glm::vec3(end.x, end.y, depth),
            uv: uv_max,
            color: col.to_vec(),
        },
    ]
}
