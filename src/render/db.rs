use super::*;
use crate::entity;
use crate::table::{TableVisitablePerId, TableVisitorPerId};
use dpx_db::mapvectable::MapVecTable;
use dpx_db::table;
use std::num::NonZeroUsize;

/// a renderer is a polymorphic object
pub type RendererId = NonZeroUsize;
pub type TRenderer = MapVecTable<RendererId, Box<dyn Renderer>, table::ManualIdTag>;

/// a render pass
#[derive(Debug, Clone, Copy)]
pub struct RenderPass {
    pub viewport: ViewportId,
    pub renderer: RendererId,
    pub order: i32,
    pub camera: CameraId,
}
pub type RenderPassId = NonZeroUsize;
pub type TRenderPass = MapVecTable<RenderPassId, RenderPass, table::AutoIdTag>;

pub type ViewportId = NonZeroUsize;
pub type TViewport = MapVecTable<ViewportId, Viewport, table::AutoIdTag>;

pub type CameraId = NonZeroUsize;
pub type TCamera = MapVecTable<CameraId, Camera, table::AutoIdTag>;

pub type TDrawableShader = MapVecTable<entity::EntityId, DrawableShader, table::ManualIdTag>;
pub type TDrawableColor = MapVecTable<entity::EntityId, DrawableColor, table::ManualIdTag>;

pub type TextureId = NonZeroUsize;
pub type TTexture = MapVecTable<TextureId, crate::gl::Texture, table::AutoIdTag>;

pub type ShaderId = NonZeroUsize;
pub type TShader = MapVecTable<ShaderId, crate::gl::Shader, table::AutoIdTag>;

pub type FontId = NonZeroUsize;
pub type TFont = MapVecTable<FontId, Font, table::AutoIdTag>;

pub type UniformLocationId = NonZeroUsize;
pub type TUniformLocation = MapVecTable<UniformLocationId, UniformLocation, table::AutoIdTag>;

//#[derive(DpxDbModule)]
//#[db_tables(TRenderer, TRenderPass)]
/// contains the rendering tables common to all rendering
pub struct CoreModule {
    pub renderer_t: TRenderer,
    pub render_pass_t: TRenderPass,
    pub viewport_t: TViewport,
    pub camera_t: TCamera,
    pub drawable_shader_t: TDrawableShader,
    pub drawable_color_t: TDrawableColor,
    pub shader_t: TShader,
    pub texture_t: TTexture,
    pub uniform_location_t: TUniformLocation,
}

impl CoreModule {
    pub fn new() -> Self {
        Self {
            renderer_t: TRenderer::new(),
            render_pass_t: TRenderPass::new(),
            viewport_t: TViewport::new(),
            camera_t: TCamera::new(),
            drawable_shader_t: TDrawableShader::new(),
            drawable_color_t: TDrawableColor::new(),
            shader_t: TShader::new(),
            texture_t: TTexture::new(),
            uniform_location_t: TUniformLocation::new(),
        }
    }
}

impl TableVisitablePerId<entity::EntityId> for CoreModule {
    fn visit_manual_id_mut<Visitor: TableVisitorPerId<entity::EntityId>>(
        &mut self,
        visitor: &Visitor,
    ) {
        visitor.visit_manual_id_mut(&mut self.drawable_color_t);
        visitor.visit_manual_id_mut(&mut self.drawable_shader_t);
    }
    fn visit_manual_id<Visitor: TableVisitorPerId<entity::EntityId>>(&self, visitor: &Visitor) {
        visitor.visit_manual_id(&self.drawable_color_t);
        visitor.visit_manual_id(&self.drawable_shader_t);
    }
}
