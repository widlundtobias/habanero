use super::*;
use dpx_db::table::AutoId;
use std::ffi::CString;

pub struct UniformLocation {
    pub shader_program_handle: crate::gl::types::GLuint,
    pub uniform_name: String,
    pub location: i32,
}

impl UniformLocation {
    pub fn cached_get<'a>(
        shader: &crate::gl::Shader,
        uniform_name: &str,
        uniform_location_t: &'a mut TUniformLocation,
        gl: &crate::gl::Gl,
    ) -> &'a UniformLocation {
        let exists = match uniform_location_t.iter().find(|u| {
            u.data.shader_program_handle == shader.program_gl_handle
                && u.data.uniform_name == uniform_name
        }) {
            Some(_) => true,
            None => false,
        };

        if exists {
            return uniform_location_t
                .iter()
                .find(|u| {
                    u.data.shader_program_handle == shader.program_gl_handle
                        && u.data.uniform_name == uniform_name
                })
                .unwrap()
                .data;
        } else {
            let c_uniform_name = CString::new(uniform_name).unwrap();

            let location =
                unsafe { gl.GetUniformLocation(shader.program_gl_handle, c_uniform_name.as_ptr()) };

            return uniform_location_t
                .insert(UniformLocation {
                    location,
                    uniform_name: String::from(uniform_name),
                    shader_program_handle: shader.program_gl_handle,
                })
                .data;
        }
    }
}
