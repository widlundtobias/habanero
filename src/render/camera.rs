#[derive(Debug, Clone, Copy)]
pub struct Perspective {
    pub vertical_fov: f32,
    pub aspect: f32,
    pub near: f32,
    pub far: f32,
}
#[derive(Debug, Clone, Copy)]
pub struct Orthographic {
    pub top: f32,
    pub bottom: f32,
    pub left: f32,
    pub right: f32,
    pub near: f32,
    pub far: f32,
}
#[derive(Debug, Clone, Copy)]
pub enum Projection {
    Perspective(Perspective),
    Orthographic(Orthographic),
}

impl Projection {
    pub fn new_perspective(vertical_fov: f32, aspect: f32, near: f32, far: f32) -> Self {
        Projection::Perspective(Perspective {
            vertical_fov,
            aspect,
            near,
            far,
        })
    }

    pub fn new_orthographic(
        top: f32,
        bottom: f32,
        left: f32,
        right: f32,
        near: f32,
        far: f32,
    ) -> Self {
        Projection::Orthographic(Orthographic {
            top,
            bottom,
            left,
            right,
            near,
            far,
        })
    }
}

impl Projection {
    fn matrix(&self) -> glm::Mat4 {
        match &self {
            Projection::Perspective(pers) => {
                glm::perspective(pers.aspect, pers.vertical_fov, pers.near, pers.far)
            }
            Projection::Orthographic(orth) => glm::ortho(
                orth.left,
                orth.right,
                orth.bottom,
                orth.top,
                orth.near,
                orth.far,
            ),
        }
    }
}

impl From<Projection> for glm::Mat4 {
    fn from(p: Projection) -> Self {
        p.matrix()
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Camera {
    pub translation: glm::Vec3,
    pub orientation: glm::Quat,
    pub projection: Projection,
}

impl Camera {
    pub fn new(projection: Projection) -> Self {
        Self {
            translation: glm::zero(),
            orientation: glm::quat_identity(),
            projection,
        }
    }
    pub fn with_translation(translation: glm::Vec3, projection: Projection) -> Self {
        Self {
            translation,
            orientation: glm::quat_identity(),
            projection,
        }
    }

    pub fn view_matrix(&self) -> glm::Mat4 {
        let direction = glm::vec3(0.0, 0.0, -1.0);
        let direction = glm::quat_rotate_vec3(&self.orientation, &direction);
        let look_at_p = self.translation + direction;
        glm::look_at(&self.translation, &look_at_p, &glm::vec3(0.0, 1.0, 0.0))
    }

    pub fn view_projection_matrix(&self) -> glm::Mat4 {
        return self.projection.matrix() * self.view_matrix();
    }
}
