use super::*;
use crate::gl;

#[derive(Debug, Copy, Clone)]
pub enum PrimitiveType {
    Triangles,
    TriangleStrip,
    TriangleFan,
    Points,
    Lines,
    LineStrip,
    LineLoop,
}

impl PrimitiveType {
    pub fn gl_enum(&self) -> crate::gl::types::GLenum {
        match self {
            PrimitiveType::Triangles => gl::TRIANGLES,
            PrimitiveType::TriangleStrip => gl::TRIANGLE_STRIP,
            PrimitiveType::TriangleFan => gl::TRIANGLE_FAN,
            PrimitiveType::Points => gl::POINTS,
            PrimitiveType::Lines => gl::LINES,
            PrimitiveType::LineStrip => gl::LINE_STRIP,
            PrimitiveType::LineLoop => gl::LINE_LOOP,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum PrimitiveClass {
    Triangles,
    Lines,
    Points,
}

impl From<PrimitiveType> for PrimitiveClass {
    fn from(p: PrimitiveType) -> Self {
        match p {
            PrimitiveType::TriangleFan
            | PrimitiveType::Triangles
            | PrimitiveType::TriangleStrip => PrimitiveClass::Triangles,
            PrimitiveType::LineLoop | PrimitiveType::LineStrip | PrimitiveType::Lines => {
                PrimitiveClass::Lines
            }
            PrimitiveType::Points => PrimitiveClass::Points,
        }
    }
}
pub trait PrimitiveList<T: VertexStorage> {
    fn primitive_type(&self) -> PrimitiveType;
    fn vertices(&self) -> &T;
    fn vertices_mut(&mut self) -> &mut T;
    fn into_vertices(self) -> T;
}

pub struct TrianglesPrimitiveList<T: VertexStorage> {
    vertices: T,
}

impl<T: VertexStorage> TrianglesPrimitiveList<T> {
    pub fn new() -> Self {
        Self { vertices: T::new() }
    }
    pub fn new_with_vertex_count(size: usize) -> Self {
        Self {
            vertices: T::new_with_count(size),
        }
    }
}

impl<T: VertexStorage> PrimitiveList<T> for TrianglesPrimitiveList<T> {
    fn primitive_type(&self) -> PrimitiveType {
        PrimitiveType::Triangles
    }
    fn vertices(&self) -> &T {
        &self.vertices
    }
    fn vertices_mut(&mut self) -> &mut T {
        &mut self.vertices
    }
    fn into_vertices(self) -> T {
        self.vertices
    }
}

pub struct TriangleStripPrimitiveList<T: VertexStorage> {
    vertices: T,
}

impl<T: VertexStorage> TriangleStripPrimitiveList<T> {
    pub fn new() -> Self {
        Self { vertices: T::new() }
    }
    pub fn new_with_vertex_count(size: usize) -> Self {
        Self {
            vertices: T::new_with_count(size),
        }
    }
}
impl<T: VertexStorage> PrimitiveList<T> for TriangleStripPrimitiveList<T> {
    fn primitive_type(&self) -> PrimitiveType {
        PrimitiveType::TriangleStrip
    }
    fn vertices(&self) -> &T {
        &self.vertices
    }
    fn vertices_mut(&mut self) -> &mut T {
        &mut self.vertices
    }
    fn into_vertices(self) -> T {
        self.vertices
    }
}

pub struct TriangleFanPrimitiveList<T: VertexStorage> {
    vertices: T,
}

impl<T: VertexStorage> TriangleFanPrimitiveList<T> {
    pub fn new() -> Self {
        Self { vertices: T::new() }
    }
    pub fn new_with_vertex_count(size: usize) -> Self {
        Self {
            vertices: T::new_with_count(size),
        }
    }
}
impl<T: VertexStorage> PrimitiveList<T> for TriangleFanPrimitiveList<T> {
    fn primitive_type(&self) -> PrimitiveType {
        PrimitiveType::TriangleFan
    }
    fn vertices(&self) -> &T {
        &self.vertices
    }
    fn vertices_mut(&mut self) -> &mut T {
        &mut self.vertices
    }
    fn into_vertices(self) -> T {
        self.vertices
    }
}

pub struct PointsPrimitiveList<T: VertexStorage> {
    vertices: T,
}

impl<T: VertexStorage> PointsPrimitiveList<T> {
    pub fn new() -> Self {
        Self { vertices: T::new() }
    }
    pub fn new_with_vertex_count(size: usize) -> Self {
        Self {
            vertices: T::new_with_count(size),
        }
    }
}
impl<T: VertexStorage> PrimitiveList<T> for PointsPrimitiveList<T> {
    fn primitive_type(&self) -> PrimitiveType {
        PrimitiveType::Points
    }
    fn vertices(&self) -> &T {
        &self.vertices
    }
    fn vertices_mut(&mut self) -> &mut T {
        &mut self.vertices
    }
    fn into_vertices(self) -> T {
        self.vertices
    }
}

pub struct LinesPrimitiveList<T: VertexStorage> {
    vertices: T,
}

impl<T: VertexStorage> LinesPrimitiveList<T> {
    pub fn new() -> Self {
        Self { vertices: T::new() }
    }
    pub fn new_with_vertex_count(size: usize) -> Self {
        Self {
            vertices: T::new_with_count(size),
        }
    }
}
impl<T: VertexStorage> PrimitiveList<T> for LinesPrimitiveList<T> {
    fn primitive_type(&self) -> PrimitiveType {
        PrimitiveType::Lines
    }
    fn vertices(&self) -> &T {
        &self.vertices
    }
    fn vertices_mut(&mut self) -> &mut T {
        &mut self.vertices
    }
    fn into_vertices(self) -> T {
        self.vertices
    }
}

pub struct LineStripPrimitiveList<T: VertexStorage> {
    vertices: T,
}

impl<T: VertexStorage> LineStripPrimitiveList<T> {
    pub fn new() -> Self {
        Self { vertices: T::new() }
    }
    pub fn new_with_vertex_count(size: usize) -> Self {
        Self {
            vertices: T::new_with_count(size),
        }
    }
}
impl<T: VertexStorage> PrimitiveList<T> for LineStripPrimitiveList<T> {
    fn primitive_type(&self) -> PrimitiveType {
        PrimitiveType::LineStrip
    }
    fn vertices(&self) -> &T {
        &self.vertices
    }
    fn vertices_mut(&mut self) -> &mut T {
        &mut self.vertices
    }
    fn into_vertices(self) -> T {
        self.vertices
    }
}

pub struct LineLoopPrimitiveList<T: VertexStorage> {
    vertices: T,
}

impl<T: VertexStorage> LineLoopPrimitiveList<T> {
    pub fn new() -> Self {
        Self { vertices: T::new() }
    }
    pub fn new_with_vertex_count(size: usize) -> Self {
        Self {
            vertices: T::new_with_count(size),
        }
    }
}
impl<T: VertexStorage> PrimitiveList<T> for LineLoopPrimitiveList<T> {
    fn primitive_type(&self) -> PrimitiveType {
        PrimitiveType::LineLoop
    }
    fn vertices(&self) -> &T {
        &self.vertices
    }
    fn vertices_mut(&mut self) -> &mut T {
        &mut self.vertices
    }
    fn into_vertices(self) -> T {
        self.vertices
    }
}

/////primitives drawing

pub struct VertexUploadData<'a> {
    pub source: &'a [u8],
    pub target: &'a crate::gl::ArrayVbo,
}

pub fn draw_primitives_vbo<U: VertexStorage, T: PrimitiveList<U>>(
    primlist: &T,
    upload_data: &[VertexUploadData],
    usage: crate::gl::types::GLenum,
    vertex_count: usize,
    gl: &crate::gl::Gl,
) {
    assert!(vertex_count > 0);

    for upload in upload_data {
        upload.target.upload_array_vbo_raw(usage, upload.source, gl);
    }

    unsafe {
        gl.DrawArrays(primlist.primitive_type().gl_enum(), 0, vertex_count as i32);
    }
}

impl<'a> VertexUploadData<'a> {
    pub fn new<T: VertexAttributeTag, U: VertexAttributeStorage<T>>(
        vbo: &'a crate::gl::ArrayVbo,
        vertex_storage: &'a U,
        start: usize,
        count: usize,
    ) -> Self {
        Self {
            target: vbo,
            source: <_ as VertexAttributeStorage<T>>::get_slice_raw(
                vertex_storage,
                start..(start + count),
            ),
        }
    }
}

#[derive(Clone)]
pub struct DynamicPrimitiveList<T: VertexStorage> {
    primitive_type: PrimitiveType,
    vertices: T,
}

impl<T: VertexStorage> DynamicPrimitiveList<T> {
    pub fn new(primitive_type: PrimitiveType) -> Self {
        Self {
            primitive_type,
            vertices: T::new(),
        }
    }
    pub fn new_with_vertex_count(primitive_type: PrimitiveType, size: usize) -> Self {
        Self {
            primitive_type,
            vertices: T::new_with_count(size),
        }
    }
}

impl<T: VertexStorage> PrimitiveList<T> for DynamicPrimitiveList<T> {
    fn primitive_type(&self) -> PrimitiveType {
        self.primitive_type
    }
    fn vertices(&self) -> &T {
        &self.vertices
    }
    fn vertices_mut(&mut self) -> &mut T {
        &mut self.vertices
    }
    fn into_vertices(self) -> T {
        self.vertices
    }
}
