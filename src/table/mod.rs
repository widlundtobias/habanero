use super::hash::TypeHash;
use dpx_db::table;

pub trait TableVisitorPerId<Id: table::TableId> {
    //these shouldn't be std::clone::Clone, but not sure how to solve it proper......
    fn visit_auto_id_mut<T: std::clone::Clone + TypeHash + 'static>(
        &self,
        _table: &mut impl table::AutoId<Id = Id, TableItem = T>,
    ) {
    }
    fn visit_manual_id_mut<T: std::clone::Clone + TypeHash + 'static>(
        &self,
        _table: &mut impl table::ManualId<Id = Id, TableItem = T>,
    ) {
    }
    fn visit_auto_id<T: std::clone::Clone + TypeHash + 'static>(
        &self,
        _table: &impl table::AutoId<Id = Id, TableItem = T>,
    ) {
    }
    fn visit_manual_id<T: std::clone::Clone + TypeHash + 'static>(
        &self,
        _table: &impl table::ManualId<Id = Id, TableItem = T>,
    ) {
    }
}

pub trait TableVisitablePerId<Id: table::TableId> {
    fn visit_auto_id_mut<Visitor: TableVisitorPerId<Id>>(&mut self, _visitor: &Visitor) {}
    fn visit_auto_id<Visitor: TableVisitorPerId<Id>>(&self, _visitor: &Visitor) {}
    fn visit_manual_id_mut<Visitor: TableVisitorPerId<Id>>(&mut self, _visitor: &Visitor) {}
    fn visit_manual_id<Visitor: TableVisitorPerId<Id>>(&self, _visitor: &Visitor) {}
}
