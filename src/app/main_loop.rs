pub enum MainLoopEvent {
    Continue,
    Quit,
}

#[cfg(target_os = "emscripten")]
use super::emscripten::*;

pub trait MainLoop {
    fn main_loop(&mut self) -> MainLoopEvent;
}

pub fn run<App: MainLoop>(mut app: App) {
    #[cfg(not(target_os = "emscripten"))]
    let quit_cell = std::cell::RefCell::new(false);
    #[cfg(not(target_os = "emscripten"))]
    let should_quit = || *quit_cell.borrow();
    let quit = || {
        #[cfg(not(target_os = "emscripten"))]
        {
            *quit_cell.borrow_mut() = true;
        }

        #[cfg(target_os = "emscripten")]
        emscripten::cancel_main_loop();
    };

    #[cfg(target_os = "emscripten")]
    let loop_closure;
    #[cfg(not(target_os = "emscripten"))]
    let mut loop_closure;

    loop_closure = move || match app.main_loop() {
        MainLoopEvent::Continue => {}
        MainLoopEvent::Quit => quit(),
    };

    #[cfg(target_os = "emscripten")]
    emscripten::set_main_loop_callback(loop_closure);

    #[cfg(not(target_os = "emscripten"))]
    loop {
        loop_closure();
        if should_quit() {
            break;
        }
    }
}
