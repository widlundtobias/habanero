// taken from https://github.com/Gigoteur/PX8/blob/master/src/px8/emscripten.rs

#[cfg(target_os = "emscripten")]
pub mod emscripten {
    use std::cell::RefCell;
    use std::os::raw::{c_int, c_void};
    use std::ptr::null_mut;

    #[allow(non_camel_case_types)]
    type em_callback_func = unsafe extern "C" fn();

    extern "C" {
        pub fn emscripten_set_main_loop(
            func: em_callback_func,
            fps: c_int,
            simulate_infinite_loop: c_int,
        );
        pub fn emscripten_cancel_main_loop();
    //pub fn emscripten_get_now() -> c_float;
    }

    thread_local! {
        static MAIN_LOOP_CLOSURE: RefCell<*mut c_void> = RefCell::new(null_mut());
        static DROP_MAIN_LOOP_CLOSURE: RefCell<Option<Box<dyn FnOnce() -> ()>>> = RefCell::new(None);
    }

    pub fn set_main_loop_callback<F>(callback: F)
    where
        F: FnMut(),
    {
        //check if we are already scheduling something
        DROP_MAIN_LOOP_CLOSURE.with(|d| {
            let nothing_scheduled = d.borrow_mut().is_none();
            if !nothing_scheduled {
                panic!("cannot set main loop callback when there is already one set");
            }
        });

        let callback = Box::leak(Box::new(callback));
        MAIN_LOOP_CLOSURE.with(|log| {
            *log.borrow_mut() = callback as *const _ as *mut c_void;
        });
        DROP_MAIN_LOOP_CLOSURE.with(|d| {
            *d.borrow_mut() = Some(Box::new(|| {
                MAIN_LOOP_CLOSURE.with(|c| {
                    let closure = *c.borrow_mut() as *mut F;
                    unsafe {
                        Box::from_raw(closure); //this drops
                    }

                    *c.borrow_mut() = null_mut();
                });
            }));
        });

        unsafe {
            emscripten_set_main_loop(wrapper::<F>, 0, 1);
        }

        unsafe extern "C" fn wrapper<F>()
        where
            F: FnMut(),
        {
            MAIN_LOOP_CLOSURE.with(|z| {
                let closure = *z.borrow_mut() as *mut F;
                (*closure)();
            });
        }
    }

    pub fn cancel_main_loop() {
        unsafe {
            emscripten_cancel_main_loop();
        }
        DROP_MAIN_LOOP_CLOSURE.with(|d| {
            let contained = std::mem::replace(&mut (*d.borrow_mut()), None);
            match contained {
                Some(f) => f(),
                None => panic!("cannot cancel when no main loop is set"),
            };
        });
    }
}
