use super::*;
pub struct Texture {
    pub gl_handle: u32,
    pub size: glm::UVec2,
    gl: std::rc::Rc<Gl>,
}

pub enum WrapMode {
    Clamp,
    Repeat,
    MirrorRepeat,
}
pub enum FilterMode {
    Nearest,
    Linear,
    MipMap,
}

pub struct TextureConfiguration {
    pub generate_mipmaps: bool,
    pub wrap_mode: WrapMode,
    pub filter_mode: FilterMode,
}

impl Texture {
    pub fn new_without_data(gl: std::rc::Rc<Gl>) -> Texture {
        let mut gl_handle: types::GLuint = 0;
        unsafe {
            gl.GenTextures(1, &mut gl_handle as *mut _);
        }
        assert!(gl_handle > 0);

        Texture {
            gl_handle,
            size: glm::vec2(0, 0),
            gl,
        }
    }

    pub fn new_rgb_from_color(
        col: crate::color::Color,
        config: &TextureConfiguration,
        gl: std::rc::Rc<Gl>,
    ) -> Texture {
        //create our texture from a 1x1 image with the given color
        let mut image = image::RgbImage::new(1, 1);
        image.put_pixel(0, 0, image::Rgb([col.r, col.g, col.b]));

        let size = glm::vec2(image.width(), image.height());
        let pixels = image.as_ptr() as *const std::ffi::c_void;
        Texture::new_helper(config, RGB, RGB8, size, pixels, gl)
    }

    pub fn new_rgba_from_color(
        col: crate::color::Color,
        config: &TextureConfiguration,
        gl: std::rc::Rc<Gl>,
    ) -> Texture {
        //create our texture from a 1x1 image with the given color
        let mut image = image::RgbaImage::new(1, 1);
        image.put_pixel(0, 0, image::Rgba([col.r, col.g, col.b, col.a]));

        let size = glm::vec2(image.width(), image.height());
        let pixels = image.as_ptr() as *const std::ffi::c_void;

        Texture::new_helper(config, RGBA, RGBA8, size, pixels, gl)
    }
    pub fn new_rgb_from_image(
        img: &image::RgbImage,
        config: &TextureConfiguration,
        gl: std::rc::Rc<Gl>,
    ) -> Texture {
        let size = glm::vec2(img.width(), img.height());
        let pixels = img.as_ptr() as *const std::ffi::c_void;
        Texture::new_helper(config, RGB, RGB8, size, pixels, gl)
    }

    pub fn new_rgba_from_image(
        img: &image::RgbaImage,
        config: &TextureConfiguration,
        gl: std::rc::Rc<Gl>,
    ) -> Texture {
        let size = glm::vec2(img.width(), img.height());
        let pixels = img.as_ptr() as *const std::ffi::c_void;
        Texture::new_helper(config, RGBA, RGBA8, size, pixels, gl)
    }

    pub fn new_helper(
        config: &TextureConfiguration,
        format: types::GLenum,
        internal_format: types::GLenum,
        size: glm::UVec2,
        pixels: *const std::ffi::c_void,
        gl: std::rc::Rc<Gl>,
    ) -> Texture {
        let mut result = Texture::new_without_data(gl.clone());
        result.size = size;

        result.bind(TEXTURE_2D, 0, &gl);

        //not sure if this one has to be before TexImage2D or not, but it was in a tutorial online
        if !config.generate_mipmaps {
            unsafe {
                gl.TexParameteri(TEXTURE_2D, TEXTURE_BASE_LEVEL, 0);
                gl.TexParameteri(TEXTURE_2D, TEXTURE_MAX_LEVEL, 0);
            }
        }

        //upload data
        unsafe {
            gl.TexImage2D(
                TEXTURE_2D,
                0,
                internal_format as i32,
                size.x as i32,
                size.y as i32,
                0,
                format,
                UNSIGNED_BYTE,
                pixels,
            );
        }

        //filter mode
        unsafe {
            gl.TexParameteri(
                TEXTURE_2D,
                TEXTURE_MIN_FILTER,
                match config.filter_mode {
                    FilterMode::Nearest => NEAREST,
                    FilterMode::Linear => LINEAR,
                    FilterMode::MipMap => NEAREST_MIPMAP_LINEAR,
                } as i32,
            );
            gl.TexParameteri(
                TEXTURE_2D,
                TEXTURE_MAG_FILTER,
                match config.filter_mode {
                    FilterMode::Nearest => NEAREST,
                    FilterMode::Linear => LINEAR,
                    FilterMode::MipMap => LINEAR,
                } as i32,
            );
        }

        //wrap mode
        let wrap_mode = match config.wrap_mode {
            WrapMode::Clamp => CLAMP_TO_EDGE,
            WrapMode::Repeat => REPEAT,
            WrapMode::MirrorRepeat => MIRRORED_REPEAT,
        } as i32;
        unsafe {
            gl.TexParameteri(TEXTURE_2D, TEXTURE_WRAP_S, wrap_mode);
            gl.TexParameteri(TEXTURE_2D, TEXTURE_WRAP_T, wrap_mode);
        }

        //mip maps
        if config.generate_mipmaps {
            unsafe {
                gl.GenerateMipmap(TEXTURE_2D);
            }
        }

        //done
        Texture::unbind(TEXTURE_2D, 0, &gl);

        result
    }

    pub fn bind(&self, target: super::types::GLenum, texture_unit: i32, gl: &super::Gl) {
        unsafe {
            gl.ActiveTexture(Self::texture_unit(texture_unit));
            gl.BindTexture(target, self.gl_handle);
        }
    }

    pub fn unbind(target: super::types::GLenum, texture_unit: i32, gl: &super::Gl) {
        unsafe {
            gl.ActiveTexture(Self::texture_unit(texture_unit));
            gl.BindTexture(target, 0);
        }
    }

    pub fn texture_unit(num: i32) -> types::GLenum {
        match num {
            0 => TEXTURE0,
            1 => TEXTURE1,
            2 => TEXTURE2,
            3 => TEXTURE3,
            4 => TEXTURE4,
            5 => TEXTURE5,
            6 => TEXTURE6,
            7 => TEXTURE7,
            8 => TEXTURE8,
            9 => TEXTURE9,
            _ => unimplemented!("unsupported texture unit"),
        }
    }
}

impl Drop for Texture {
    fn drop(&mut self) {
        unsafe {
            self.gl.DeleteTextures(1, &self.gl_handle as *const _);
        }
    }
}
